package ec.di.uoa.gr.mobilizr;

import android.app.Application;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.ApplicationTestCase;

import android.test.suitebuilder.TestSuiteBuilder;

import junit.framework.Assert;
import junit.framework.Test;
import junit.framework.TestSuite;

import java.util.HashSet;

import ec.di.uoa.gr.mobilizr.data.MobilizrContract;
import ec.di.uoa.gr.mobilizr.data.MobilizrDbHelper;

public class ApplicationTest extends TestSuite {
    public static Test suite() {
        return (Test) new TestSuiteBuilder(ApplicationTest.class)
                .includeAllPackagesUnderHere()
                .includePackages(".data");
    }
    private Context mContext = null;

    // Since we want each test to start with a clean slate
//    void deleteTheDatabase() {
//        mContext.deleteDatabase(MobilizrDbHelper.class.);
//    }

    public ApplicationTest() {
        super();
    }
}