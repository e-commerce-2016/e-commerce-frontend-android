package ec.di.uoa.gr.mobilizr.shops.entities;

import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import ec.di.uoa.gr.mobilizr.products.product.entities.Product;

public class TestProductShopsBundle {
    private Product product;
    private ShopHasProduct shopHasProduct;
    private ShopHasProductAggregated shopHasProductAggregated;
    private ProductShopsBundle productShopsBundle;

    @Before
    public void initialize() {
        product = new Product();
        shopHasProduct = new ShopHasProduct();
        shopHasProductAggregated = new ShopHasProductAggregated();

        ArrayList<ShopHasProductAggregated> shopHasProductAggregatedArrayList = new ArrayList<>();
        shopHasProductAggregatedArrayList.add(shopHasProductAggregated);
        shopHasProductAggregatedArrayList.add(shopHasProductAggregated);
        productShopsBundle = new ProductShopsBundle(product, shopHasProductAggregatedArrayList);
    }

    @Test
    public void testShopHasProductSerializability() throws IOException {
        new ObjectOutputStream(new ByteArrayOutputStream()).writeObject(shopHasProduct);
    }

    @Test
    public void testShopHasProductAggregatedSerializability() throws IOException {
        new ObjectOutputStream(new ByteArrayOutputStream()).writeObject(shopHasProductAggregated);
    }

    @Test
    public void testProductShopsBundleSerializability() throws IOException {
        new ObjectOutputStream(new ByteArrayOutputStream()).writeObject(productShopsBundle);
    }
}
