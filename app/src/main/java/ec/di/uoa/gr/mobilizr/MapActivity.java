package ec.di.uoa.gr.mobilizr;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SlidingPaneLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Spanned;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ec.di.uoa.gr.mobilizr.map.DirectionFinder;
import ec.di.uoa.gr.mobilizr.map.DirectionFinderListener;
import ec.di.uoa.gr.mobilizr.map.Route;
import ec.di.uoa.gr.mobilizr.util.UiUtils;
import timber.log.Timber;

public class MapActivity extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        OnMapReadyCallback, LocationListener, LocationSource, DirectionFinderListener {
    /**
     * Constant used in the location settings dialog.
     */
    protected static final int REQUEST_CHECK_SETTINGS = 0x1;

    private final float STREET_VIEW_LAUNCH_DISTANCE = 20;
    @BindView(R.id.driving_directions)
    ImageButton drivingDirectionsButton;
    @BindView(R.id.walking_directions)
    ImageButton walkingDirectionsButton;
    @BindView(R.id.map_view_container_layout)
    RelativeLayout mapViewContainerLayout;
    @BindView(R.id.map_view_directions_list_view)
    ListView mapViewDirectionsListView;
    @BindView(R.id.show_directions_button)
    FloatingActionButton showDirectionsButton;
    @BindView(R.id.map_view_map_container_layout)
    SlidingPaneLayout mapViewMapContainerPaneLayout;
    private GoogleMap mMap;
    private LocationManager locationManager;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION_INFO = 1;
    private Location mLastLocation;
    private Location mCurrentLocation;
    private GoogleApiClient mGoogleApiClient;
    private List<Polyline> polylinePaths = new ArrayList<>();
    private List<Spanned> directionsList = new ArrayList<>();
    private ProgressDialog progressDialog;
    private OnLocationChangedListener mListener;
    private LocationRequest mLocationRequest;
    private Boolean mRequestingLocationUpdates = true;
    private long mWishlistItemId;
    private LatLng mShopLatLng;
    private Location mShopLocation;
    private String mShopBrand;
    private String mShopInfo;
    private ArrayAdapter<Spanned> directionsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setProgressBarIndeterminate(true);
        setContentView(R.layout.activity_map);
        ButterKnife.bind(this);

        MobilizrApplication.getsActionBar().setTitle(R.string.map_activity_title);
        MobilizrApplication.getsActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        mWishlistItemId = intent.getLongExtra(getString(R.string.wishlist_item_id_for_review_key), -1);
        mShopLatLng = intent.getParcelableExtra(getString(R.string.wishlist_item_shop_latlng_key));
        mShopBrand = intent.getStringExtra(getString(R.string.wishlist_item_shop_name_key));
        mShopInfo = intent.getStringExtra(getString(R.string.wishlist_item_shop_info_key));

        mShopLocation = new Location(LocationManager.PASSIVE_PROVIDER);
        mShopLocation.setLatitude(mShopLatLng.latitude);
        mShopLocation.setLongitude(mShopLatLng.longitude);

        directionsAdapter = new ArrayAdapter<Spanned>(this, R.layout.simple_list_item_1, directionsList);
        mapViewDirectionsListView.setAdapter(directionsAdapter);

        //setup location service
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        mLocationRequest = new LocationRequest();
        long fastestInterval = 10 * 1000; // 10 seconds update rate should be fine
        mLocationRequest.setInterval(fastestInterval);
        mLocationRequest.setFastestInterval(fastestInterval); // Capping the rate.
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient,
                        builder.build());

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                LinearLayout mRoot = (LinearLayout) findViewById(R.id.mainlin);
                Snackbar bar = Snackbar.make(mRoot, "Your location is unavailable at the moment", Snackbar.LENGTH_LONG);
                Timber.v("ShowRequestPermissions");
                bar.show();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION_INFO);
                Timber.v("ElseShowRequestPermissions");
            }

        } else {
            locationManager = (LocationManager) getSystemService
                    (Context.LOCATION_SERVICE);
            mCurrentLocation = locationManager.getLastKnownLocation
                    (LocationManager.PASSIVE_PROVIDER);
        }
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setLocationSource(this);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                LinearLayout mRoot = (LinearLayout) findViewById(R.id.mainlin);
                Snackbar bar = Snackbar.make(mRoot, "Your location is unavailable at the moment", Snackbar.LENGTH_LONG);
                bar.show();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION_INFO);
            }

        } else {
            mMap.setMyLocationEnabled(true);
            locationManager = (LocationManager) getSystemService
                    (Context.LOCATION_SERVICE);
            Location location = locationManager.getLastKnownLocation
                    (LocationManager.PASSIVE_PROVIDER);

            UiSettings mapSettings = mMap.getUiSettings();
            mMap.setMyLocationEnabled(true);
            mapSettings.setZoomControlsEnabled(true);
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            LatLng myLocation = null;

            if (mLastLocation != null) {
                myLocation = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            } else if (location != null) {
                // Getting latitude of the current location
                double latitude = location.getLatitude();
                // Getting longitude of the current location
                double longitude = location.getLongitude();
                myLocation = new LatLng(latitude, longitude);
            }

            if (myLocation != null) {
                try {
                    LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    builder.include(myLocation);
                    builder.include(mShopLatLng);
                    googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), 17));
                    new DirectionFinder(this, myLocation, mShopLatLng, 1).execute();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 15));
                mMap.addMarker(new MarkerOptions().position(myLocation).title("You are here"));
                if (mShopLatLng != null) {
                    MarkerOptions markerOptions = new MarkerOptions().position(mShopLatLng).title(mShopBrand);
                    markerOptions.snippet(mShopInfo);
                    mMap.addMarker(markerOptions);
                }
            }
            else {
                Toast.makeText(this, "Your location is unavailable at the moment", Toast.LENGTH_SHORT).show();
            }
        }
    }


    @Override
    public void onConnected(Bundle connectionHint) {
        try {
            // Get a quick location and register for location updates.
            if (mCurrentLocation == null)
                mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mRequestingLocationUpdates) {
                startLocationUpdates();
            }
        } catch (SecurityException e) {
            Timber.e(e, "Could not retrieve location.");
        }
    }

    protected void startLocationUpdates() {
        try {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this).setResultCallback(new ResultCallback<Status>() {
                @Override
                public void onResult(@NonNull Status status) {
                    mRequestingLocationUpdates = true;
                }
            });
        } catch (SecurityException e) {
            Timber.e(e, "Could not request location updates.");
        }
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {
                mRequestingLocationUpdates = false;
            }
        });
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Timber.i("Connection failed: ConnectionResult.getErrorCode() = %s", connectionResult.getErrorCode());
    }

    public void onStop() {
        this.mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected() && !mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }


    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    public void activate(OnLocationChangedListener listener) {
        mListener = listener;
    }

    @Override
    public void deactivate() {
        mListener = null;
    }


    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        float distance = location.distanceTo(mShopLocation);
        if (distance < STREET_VIEW_LAUNCH_DISTANCE) {
            Intent intent = new Intent(this, StreetView.class);
            intent.putExtra(getString(R.string.wishlist_item_id_for_review_key), mWishlistItemId);
            intent.putExtra(getString(R.string.street_view_shop_location_key), mShopLatLng);
            this.startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        Timber.i("User agreed to make required location settings changes.");
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        Timber.i("User chose not to make required location settings changes.");
                        break;
                }
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient.isConnected())
            stopLocationUpdates();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.disconnect();
    }

    //-------------------- Finder Methods
    //

    @Override
    public void onDirectionFinderStart() {
        Timber.v("onDirectionFinderStart");
        progressDialog = ProgressDialog.show(this, "Please wait.",
                "Getting directions..!", true);

        if (polylinePaths != null) {
            for (Polyline polyline : polylinePaths) {
                polyline.remove();
            }
        }
    }

    @Override
    public void onDirectionFinderSuccess(List<Route> routes) {
        Timber.v("onDirectionFinderSuccess");
        if (progressDialog.isShowing())
            progressDialog.dismiss();
        polylinePaths = new ArrayList<>();

        if (routes.size() != 0) {
            for (Route route : routes) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.startLocation, 16));
                ((TextView) findViewById(R.id.tvDuration)).setText(route.duration.text);
                ((TextView) findViewById(R.id.tvDistance)).setText(route.distance.text);

                directionsList.clear();
                directionsList.addAll(route.instructions);
                PolylineOptions polylineOptions = new PolylineOptions().
                        geodesic(false).
                        color(Color.BLUE).
                        width(10);

                for (int i = 0; i < route.points.size(); i++)
                    polylineOptions.add(route.points.get(i));

                polylinePaths.add(mMap.addPolyline(polylineOptions));
            }
            directionsAdapter.notifyDataSetChanged();
        } else {
            // Inform the user for unavailability of directions
            UiUtils.snackBarNotify(mapViewContainerLayout, getString(R.string.map_no_directions_available_msg));
        }

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                Snackbar bar = Snackbar.make(mapViewContainerLayout, "Your location is unavailable at the moment", Snackbar.LENGTH_LONG);
                Timber.v("Explaining why we need location permissions.");
                bar.show();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION_INFO);
                Timber.v("Requesting location permissions.");
            }

        } else {
            locationManager = (LocationManager) getSystemService
                    (Context.LOCATION_SERVICE);
            Location location = locationManager.getLastKnownLocation
                    (LocationManager.PASSIVE_PROVIDER);

//            float distance = location.distanceTo(mShopLocation);
//            double distance = STREET_VIEW_LAUNCH_DISTANCE - 1;
//            if (distance < STREET_VIEW_LAUNCH_DISTANCE) {
//                Timber.v("Opening StreetView");
//                Intent intent = new Intent(this, StreetView.class);
//                intent.putExtra(getString(R.string.wishlist_item_id_for_review_key), mWishlistItemId);
//                intent.putExtra(getString(R.string.street_view_shop_location_key), mShopLatLng);
//                this.startActivity(intent);
//            }
        }
        setProgressBarIndeterminate(false);
    }

    private void toggleDirectionsType(int travelMode) {
        try {
            UiSettings mapSettings = mMap.getUiSettings();
            mapSettings.setZoomControlsEnabled(true);
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            LatLng currentLatLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 15));
            try {
                new DirectionFinder(this, currentLatLng, mShopLatLng, travelMode).execute();
            } catch (UnsupportedEncodingException e) {
                Timber.e(e, "Could not decode directions json.");
            }
        }
        catch (SecurityException e) {
            throw new SecurityException("Location permission isn't available.", e);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION_INFO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @OnClick({R.id.driving_directions, R.id.walking_directions})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.driving_directions:
                walkingDirectionsButton.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
                drivingDirectionsButton.setBackgroundColor(getResources().getColor(R.color.material_blue_500));
                toggleDirectionsType(DirectionFinder.TRAVEL_MODE_DRIVING);
                break;
            case R.id.walking_directions:
                drivingDirectionsButton.setBackgroundColor(getResources().getColor(android.R.color.darker_gray));
                walkingDirectionsButton.setBackgroundColor(getResources().getColor(R.color.material_blue_500));
                toggleDirectionsType(DirectionFinder.TRAVEL_MODE_WALKING);
                break;
        }
    }

    @OnClick(R.id.show_directions_button)
    public void onClick() {
        if (mapViewMapContainerPaneLayout.isOpen())
            mapViewMapContainerPaneLayout.closePane();
        else
            mapViewMapContainerPaneLayout.openPane();
    }
}
