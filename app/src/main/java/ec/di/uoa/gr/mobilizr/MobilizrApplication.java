package ec.di.uoa.gr.mobilizr;

import android.app.Application;
import android.content.Context;
import android.support.v7.app.ActionBar;

import ec.di.uoa.gr.mobilizr.data.MobilizrDbHelper;

public class MobilizrApplication extends Application {
    private static Context mContext;
    private static MobilizrDbHelper dbHelper;
    private static ActionBar sActionBar;
    // Defines a penalty to be enforced on the submitted rating
    public static final double NON_REAL_PRICE_RATING_PENALTY = 0.5;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        dbHelper = MobilizrDbHelper.getsInstance(getApplicationContext());
    }

    public static Context getContext() {
        return mContext;
    }

    public static MobilizrDbHelper getDbHelper() {
        return dbHelper;
    }

    public static ActionBar getsActionBar() {
        return sActionBar;
    }

    public static void setsActionBar(ActionBar sActionBar) {
        MobilizrApplication.sActionBar = sActionBar;
    }
}
