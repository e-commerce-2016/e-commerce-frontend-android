package ec.di.uoa.gr.mobilizr.map;

import android.os.AsyncTask;
import android.text.Html;
import android.text.Spanned;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class DirectionFinder {
    private static final String DIRECTION_URL_API = "https://maps.googleapis.com/maps/api/directions/json?";
    //private static final String GOOGLE_API_KEY =  Resources.getSystem().getString(R.string.google_maps_key);
    private DirectionFinderListener listener;
    private Double originLatitude;
    private Double originLongtitude;
    private Double destinationLatitude;
    private Double destinationLongtitude;
    private int travelMode;
    public static final int TRAVEL_MODE_DRIVING = 1;
    public static final int TRAVEL_MODE_WALKING = 2;

    public DirectionFinder(DirectionFinderListener listener,
                           LatLng origin,
                           LatLng destination,
                           int travelMode) {
        this.listener = listener;
        this.originLatitude = origin.latitude;
        this.originLongtitude = origin.longitude;
        this.destinationLatitude = destination.latitude;
        this.destinationLongtitude = destination.longitude;
        this.travelMode = travelMode;
    }

    public void execute() throws UnsupportedEncodingException {
        listener.onDirectionFinderStart();
        new DownloadRawData().execute(createUrl());
    }

    private String createUrl() throws UnsupportedEncodingException {
        Timber.v("DirectionFinder");
        String urlOriginLatitude = URLEncoder.encode(originLatitude.toString(), "utf-8");
        String urlOriginLongtitude = URLEncoder.encode(originLongtitude.toString(), "utf-8");
        String urlDestinationLatitude = URLEncoder.encode(destinationLatitude.toString(), "utf-8");
        String urlDestinationLongtitude = URLEncoder.encode(destinationLongtitude.toString(), "utf-8");
        String modeTr;
        switch (travelMode) {
            case 1:
                modeTr = "driving";
                break;
            case 2:
                modeTr = "walking";
                break;
            case 3:
                modeTr = "bicycling";
                break;
            case 4:
                modeTr = "transit_mode";
                break;
            default:
                modeTr = "driving";
                break;
        }

        return DIRECTION_URL_API + "origin=" + urlOriginLatitude + "," +
                urlOriginLongtitude + "&destination=" + urlDestinationLatitude + ","
                + urlDestinationLongtitude + "&avoid=highways&mode=" + modeTr;
    }

    private class DownloadRawData extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... params) {
            Timber.v("DownloadRawData");
            String link = params[0];
            try {
                URL url = new URL(link);
                Timber.d("Requesting directions with url: %s", link);
                InputStream is = url.openConnection().getInputStream();
                StringBuffer buffer = new StringBuffer();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));

                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line + "\n");
                }

                return buffer.toString();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String res) {
            try {
                parseJSon(res);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void parseJSon(String data) throws JSONException {
        Timber.v("parseJSon");
        if (data == null)
            return;

        List<Route> routes = new ArrayList<Route>();
        JSONObject jsonData = new JSONObject(data);
        JSONArray jsonRoutes = jsonData.getJSONArray("routes");
        for (int i = 0; i < jsonRoutes.length(); i++) {
            JSONObject jsonRoute = jsonRoutes.getJSONObject(i);
            Route route = new Route();

            JSONObject overview_polylineJson = jsonRoute.getJSONObject("overview_polyline");
            JSONArray jsonLegs = jsonRoute.getJSONArray("legs");
            JSONObject jsonLeg = jsonLegs.getJSONObject(i);
            JSONObject jsonDistance = jsonLeg.getJSONObject("distance");
            JSONObject jsonDuration = jsonLeg.getJSONObject("duration");
            JSONArray  jsonSteps = jsonLeg.getJSONArray("steps");
            JSONObject jsonStep = jsonSteps.getJSONObject(0);
            //JSONObject jsonInstructions = jsonStep.getJSONObject("html_instructions");
            JSONObject jsonEndLocation = jsonLeg.getJSONObject("end_location");
            JSONObject jsonStartLocation = jsonLeg.getJSONObject("start_location");

            route.distance = new Distance(jsonDistance.getString("text"), jsonDistance.getInt("value"));
            route.duration = new Duration(jsonDuration.getString("text"), jsonDuration.getInt("value"));
            route.endAddress = jsonLeg.getString("end_address");
            route.startAddress = jsonLeg.getString("start_address");
            route.startLocation = new LatLng(jsonStartLocation.getDouble("lat"), jsonStartLocation.getDouble("lng"));
            route.endLocation = new LatLng(jsonEndLocation.getDouble("lat"), jsonEndLocation.getDouble("lng"));
            route.points = decodePolyLine(overview_polylineJson.getString("points"));
            for (int j = 0; j < jsonSteps.length(); j++) {
                jsonStep = jsonSteps.getJSONObject(j);
                String html_instructions = jsonStep.getString("html_instructions");
                Spanned spanned_instructions = Html.fromHtml(html_instructions);
                route.instructions.add(spanned_instructions);
            }
            routes.add(route);
        }

        listener.onDirectionFinderSuccess(routes);
    }

    private List<LatLng> decodePolyLine(final String poly) {
        Timber.v("decodePolyLine");
        int len = poly.length();
        int index = 0;
        List<LatLng> decoded = new ArrayList<LatLng>();
        int lat = 0;
        int lng = 0;

        while (index < len) {
            int b;
            int shift = 0;
            int result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lat += dlat;

            shift = 0;
            result = 0;
            do {
                b = poly.charAt(index++) - 63;
                result |= (b & 0x1f) << shift;
                shift += 5;
            } while (b >= 0x20);
            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
            lng += dlng;

            decoded.add(new LatLng(
                    lat / 1E5, lng / 1E5
            ));
        }

        return decoded;
    }
}
