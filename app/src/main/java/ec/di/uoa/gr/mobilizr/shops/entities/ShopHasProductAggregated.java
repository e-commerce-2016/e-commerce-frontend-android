package ec.di.uoa.gr.mobilizr.shops.entities;


import java.io.Serializable;

public class ShopHasProductAggregated implements Serializable {
    private static final long serialVersionUID = -1654596145909378562L;
    private ShopHasProduct shopHasProduct;
    private Shop shop;

    public ShopHasProductAggregated() {
        shopHasProduct = new ShopHasProduct();
        shop = new Shop();
    }

    public ShopHasProduct getShopHasProduct() {
        return shopHasProduct;
    }

    public void setShopHasProduct(ShopHasProduct shopHasProduct) {
        this.shopHasProduct = shopHasProduct;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    @Override
    public String toString() {
        return "{" + shopHasProduct.toString() + "\t" + shop.toString() + "}";
    }
}
