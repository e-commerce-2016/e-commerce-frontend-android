package ec.di.uoa.gr.mobilizr.products.search;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import ec.di.uoa.gr.mobilizr.MainActivity;
import ec.di.uoa.gr.mobilizr.MobilizrApplication;
import ec.di.uoa.gr.mobilizr.R;
import ec.di.uoa.gr.mobilizr.products.FetchProductsTask;
import ec.di.uoa.gr.mobilizr.products.product.entities.Product;
import ec.di.uoa.gr.mobilizr.util.BackendUtils;
import ec.di.uoa.gr.mobilizr.util.OnArrayListAsyncTaskCompletedListener;
import timber.log.Timber;

public class SearchProductsView extends Fragment {
    // Delegate for the MainActivity that must implement the interface below.
    OnSearchResultsReceivedListener mOnSearchResultsReceivedListener = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_products_search, container, false);

        MobilizrApplication.getsActionBar().setTitle(R.string.toolbar_title_search);
        MobilizrApplication.getsActionBar().setDisplayHomeAsUpEnabled(false);

        // Get handle for the search button so as to set its onClickListener.
        Button searchButton = (Button) rootView.findViewById(R.id.button_search);
        searchButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // Retrieve and check keyphrase
                String keyphrase = ((TextView)rootView.findViewById(R.id.search_field_text))
                        .getText().toString();
                if (keyphrase.length() == 0) {
                    // Cannot search with empty keyphrase, inform the user
                    final Snackbar snackbar =
                            Snackbar.make(rootView, R.string.empty_keyphrase_message,
                                    Snackbar.LENGTH_INDEFINITE);

                    snackbar.setAction(R.string.dismiss_action,
                            new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            snackbar.dismiss();
                        }
                    });
                    snackbar.show();
                    return;
                }
                Timber.v("User clicked search with keyphrase: \"%s\"", keyphrase);

                // Build Uri
                Integer productsGetEndpointIdx = getResources().getInteger(R.integer.productsGETRS);
                String endpointUrl =
                        getResources().getStringArray(R.array.webServicesEntrypoints)[productsGetEndpointIdx];
                String QUERY_PARAM = getResources().getString(R.string.QUERY_PARAM);
                endpointUrl = BackendUtils.prepareEndpointURL(getContext(), endpointUrl);
                Uri builtUri = Uri.parse(endpointUrl).buildUpon()
                        .appendQueryParameter(QUERY_PARAM, keyphrase).build();

                new FetchProductsTask(getContext(),
                        new OnArrayListAsyncTaskCompletedListener<Product>() {
                    @Override
                    public void onAsyncTaskCompleted(ArrayList<Product> resultsArrayList) {
                        if (resultsArrayList == null) {
                            final Snackbar snackbar =
                                    Snackbar.make(rootView, R.string.connection_error_message,
                                            Snackbar.LENGTH_INDEFINITE);

                            snackbar.setAction(R.string.dismiss_action,
                                    new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            snackbar.dismiss();
                                        }
                                    });
                            snackbar.show();
                        }
                        else if (resultsArrayList.size() == 0) {
                            final Snackbar snackbar =
                                    Snackbar.make(rootView, R.string.no_results_message,
                                            Snackbar.LENGTH_INDEFINITE);

                            snackbar.setAction(R.string.dismiss_action,
                                    new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            snackbar.dismiss();
                                        }
                                    });
                            snackbar.show();
                        }
                        else
                            ((MainActivity) getActivity()).onSearchResultsReceived(resultsArrayList);
                    }
                }).execute(builtUri);
            }
        });

        return rootView;
    }

    // Must be implemented by parent activity in order to notify it for the search results
    // and to initiate the ProductsView fragment
    public interface OnSearchResultsReceivedListener {
        void onSearchResultsReceived(ArrayList<Product> productsList);
    }

    /**
     * Ensuring that the activity that initiated the SearchProductsView fragment implements the
     * OnSearchResultsReceivedListener interface.
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity currentActivity = getActivity();
        try {
            mOnSearchResultsReceivedListener = (OnSearchResultsReceivedListener) currentActivity;
        } catch (ClassCastException e) {
            throw new ClassCastException(currentActivity.toString() + " must implement " +
                    SearchProductsView.OnSearchResultsReceivedListener.class);
        }
    }
}
