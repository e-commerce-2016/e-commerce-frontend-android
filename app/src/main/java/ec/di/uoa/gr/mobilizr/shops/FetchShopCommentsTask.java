package ec.di.uoa.gr.mobilizr.shops;

import android.content.Context;
import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import ec.di.uoa.gr.mobilizr.R;
import ec.di.uoa.gr.mobilizr.shops.entities.Comment;
import ec.di.uoa.gr.mobilizr.util.ArrayListAsyncTask;
import ec.di.uoa.gr.mobilizr.util.JSONUnMarshaller;
import ec.di.uoa.gr.mobilizr.util.OnArrayListAsyncTaskCompletedListener;
import timber.log.Timber;

public class FetchShopCommentsTask extends ArrayListAsyncTask<Uri, Void, Comment> {

    public FetchShopCommentsTask(final Context ctx,
                                 OnArrayListAsyncTaskCompletedListener<Comment> listener) {
        super(ctx, listener, new JSONUnMarshaller<Comment>() {
            @Override
            public Comment unMarshallJSON(JSONObject json) throws JSONException {
                Comment comment = new Comment();
                comment.setId(json.getLong(ctx.getString(R.string.json_id)));
                comment.setComment(json.getString(ctx.getString(R.string.json_comment)));
                comment.setRating(json.getLong(ctx.getString(R.string.json_rating)));
                comment.setUser_id(json.getLong(ctx.getString(R.string.json_user_id)));
                comment.setShop_id(json.getLong(ctx.getString(R.string.json_shop_id)));
                String dateTimeStr = json.getString(ctx.getString(R.string.json_comment_datetime));
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                try {
                    comment.setDatetime(new java.sql.Date(format.parse(dateTimeStr).getTime()));
                } catch (ParseException e) {
                    Timber.e("Could not convert \"" + dateTimeStr + "\" to Date: %s", e.getMessage());
                }
                return comment;
            }
        });

    }
}
