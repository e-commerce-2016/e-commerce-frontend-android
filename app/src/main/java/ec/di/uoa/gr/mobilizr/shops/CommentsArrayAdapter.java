package ec.di.uoa.gr.mobilizr.shops;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import ec.di.uoa.gr.mobilizr.R;
import ec.di.uoa.gr.mobilizr.shops.entities.Comment;

public class CommentsArrayAdapter extends ArrayAdapter<Comment> {

    public CommentsArrayAdapter(Fragment clientFragment, int resource, List<Comment> objects) {
        super(clientFragment.getContext(), resource, objects);
    }

    static class ViewHolder {
        TextView comment;
        RelativeLayout details;
        RatingBar ratingBar;
        TextView date;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Comment comment = getItem(position);

        final ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.shop_view_comments_list_item, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.comment = (TextView)
                    convertView.findViewById(R.id.shop_view_comments_list_item_comment);
            viewHolder.details = (RelativeLayout)
                    convertView.findViewById(R.id.shop_view_comments_list_item_comment_details);
            viewHolder.ratingBar = (RatingBar)
                    viewHolder.details.findViewById(R.id.shop_view_comments_list_item_rating);
            viewHolder.date = (TextView)
                    viewHolder.details.findViewById(R.id.shop_view_comments_list_item_date);

            convertView.setTag(viewHolder);
        }
        else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        String commentStr = "\"" + comment.getComment() + "\"";
        viewHolder.comment.setText(commentStr);

        viewHolder.ratingBar.setRating(comment.getRating());
        viewHolder.date.setText(comment.getDatetime().toString());

        return convertView;
    }
}
