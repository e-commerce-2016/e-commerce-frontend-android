package ec.di.uoa.gr.mobilizr.data.models;

import ec.di.uoa.gr.mobilizr.products.product.entities.Product;
import ec.di.uoa.gr.mobilizr.shops.entities.Shop;

public class WishlistBundle {
    WishlistModel wishlistModel;
    Product product;
    Shop shop;

    public WishlistModel getWishlistModel() {
        return wishlistModel;
    }

    public void setWishlistModel(WishlistModel wishlistModel) {
        this.wishlistModel = wishlistModel;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }
}
