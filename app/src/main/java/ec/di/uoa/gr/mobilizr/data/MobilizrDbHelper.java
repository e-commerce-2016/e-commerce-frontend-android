package ec.di.uoa.gr.mobilizr.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ec.di.uoa.gr.mobilizr.data.models.WishlistBundle;
import ec.di.uoa.gr.mobilizr.data.models.WishlistModel;
import ec.di.uoa.gr.mobilizr.products.product.entities.Product;
import ec.di.uoa.gr.mobilizr.shops.entities.Shop;
import ec.di.uoa.gr.mobilizr.shops.entities.ShopHasProduct;
import ec.di.uoa.gr.mobilizr.shops.entities.ShopHasProductAggregated;
import timber.log.Timber;

/**
 * Using singleton pattern for MobilizrDbHelper for avoiding memory leaks and
 * unnecessary reallocations.
 * Following this: http://guides.codepath.com/android/Local-Databases-with-SQLiteOpenHelper
 */
public class MobilizrDbHelper extends SQLiteOpenHelper {
    private static final String LOG_TAG = MobilizrDbHelper.class.getCanonicalName();
    private static MobilizrDbHelper sInstance;

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static synchronized MobilizrDbHelper getsInstance(Context ctx) {
        // Using the application context ensures we don't leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null)
            sInstance = new MobilizrDbHelper(ctx.getApplicationContext());
        return sInstance;
    }

    private MobilizrDbHelper(Context context) {
        super(context, MobilizrContract.DATABASE_NAME, null, DATABASE_VERSION);
    }

    // ---------------------------------------------------------------------------------------------
    // Called when the database connection is being configured.
    // Configure database settings for things like foreign key support, write-ahead logging, etc.
    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Timber.d("Shop create SQL stmt: %s", MobilizrContract.ShopTable.SQL_CREATE_TABLE);
        db.execSQL(MobilizrContract.ShopTable.SQL_CREATE_TABLE);
        db.execSQL(MobilizrContract.ProductTable.SQL_CREATE_TABLE);
        db.execSQL(MobilizrContract.WishlistTable.SQL_CREATE_TABLE);
    }

    // Called when the database needs to be upgraded.
    // This method will only be called if a database already exists on disk with the same DATABASE_NAME,
    // but the DATABASE_VERSION is different than the version of the database that exists on disk.
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + MobilizrContract.ShopTable.TABLE_NAME);
            db.execSQL("DROP TABLE IF EXISTS " + MobilizrContract.ProductTable.TABLE_NAME);
            db.execSQL("DROP TABLE IF EXISTS " + MobilizrContract.WishlistTable.TABLE_NAME);
            onCreate(db);
        }
    }

    // CRUD Methods --------------------------------------------------------------------------------
    /**
     * Attempts to add or update an object of type T, using the pre-filled values instance.
     * <b>Uses by default CONFLICT_REPLACE conflict algorithm.</b>
     *
     * @param canonicalClassName The object to persist.
     * @param values             Must be pre-filled.
     * @param TABLE_NAME         The table to which the insert/update will occur.
     */
    private boolean addOrUpdateType(String canonicalClassName,
                                ContentValues values,
                                final String TABLE_NAME) {
        boolean wasSuccessful = false;
        SQLiteDatabase db = getWritableDatabase();

        db.beginTransaction();
        try {
            long rows = db.insertWithOnConflict(TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);

            if (rows > -1) {
                db.setTransactionSuccessful();
                wasSuccessful = true;
            } else
                throw new SQLiteException("SQL: insertWithOnConflict failed for type: "
                        + canonicalClassName);
        } catch (Exception e) {
            Timber.e("Error while trying to add/update %s: %s", canonicalClassName, e.getMessage());
        } finally {
            db.endTransaction();
        }
        return wasSuccessful;
    }

    /**
     * Attempts to add or update an object of type T, using the pre-filled values instance.
     *
     * @param canonicalClassName The object to persist.
     * @param values             Must be pre-filled.
     * @param TABLE_NAME         The table to which the insert/update will occur.
     * @param conflictAlgorithm  One of SQLiteDatabase.CONFLICT_*
     */
    private boolean addOrUpdateType(String canonicalClassName,
                                ContentValues values,
                                final String TABLE_NAME,
                                final int conflictAlgorithm) {
        if (!(conflictAlgorithm == SQLiteDatabase.CONFLICT_NONE ||
                conflictAlgorithm == SQLiteDatabase.CONFLICT_FAIL ||
                conflictAlgorithm == SQLiteDatabase.CONFLICT_REPLACE ||
                conflictAlgorithm == SQLiteDatabase.CONFLICT_ABORT ||
                conflictAlgorithm == SQLiteDatabase.CONFLICT_IGNORE ||
                conflictAlgorithm == SQLiteDatabase.CONFLICT_ROLLBACK)
                ) {
            Timber.e("Erroneous conflictAlgorithm argument.");
            return false;
        }

        boolean wasSuccessful = false;
        SQLiteDatabase db = getWritableDatabase();

        db.beginTransaction();
        try {
            long rows = db.insertWithOnConflict(TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);

            if (rows > -1) {
                db.setTransactionSuccessful();
                wasSuccessful = true;
            } else
                throw new SQLiteException("SQL: insertWithOnConflict failed for type: "
                        + canonicalClassName);
        } catch (Exception e) {
            Timber.e("Error while trying to add/update %s: %s", canonicalClassName, e.getMessage());
        } finally {
            db.endTransaction();
        }
        return wasSuccessful;
    }

    public boolean addOrUpdateShop(Shop shop) {
        ContentValues values = new ContentValues();
        values.put(MobilizrContract.ShopTable.COLUMN_ID, shop.getId());
        values.put(MobilizrContract.ShopTable.COLUMN_BRAND, shop.getBrand());
        values.put(MobilizrContract.ShopTable.COLUMN_LAT, shop.getLat());
        values.put(MobilizrContract.ShopTable.COLUMN_LON, shop.getLon());
        values.put(MobilizrContract.ShopTable.COLUMN_ADDRESS, shop.getAddress());
        values.put(MobilizrContract.ShopTable.COLUMN_CITY, shop.getCity());
        values.put(MobilizrContract.ShopTable.COLUMN_POSTAL_CODE, shop.getPostalCode());
        values.put(MobilizrContract.ShopTable.COLUMN_PHONE, shop.getPhone());
        values.put(MobilizrContract.ShopTable.COLUMN_MOBILE_PHONE, shop.getMobilePhone());
        values.put(MobilizrContract.ShopTable.COLUMN_TRN, shop.getTrn());
        values.put(MobilizrContract.ShopTable.COLUMN_RATING_SUM, shop.getRatingSum());
        values.put(MobilizrContract.ShopTable.COLUMN_RATING_COUNT, shop.getRatingCount());

        return addOrUpdateType(Shop.class.getCanonicalName(), values, MobilizrContract.ShopTable.TABLE_NAME);
    }

    public boolean addOrUpdateProduct(Product product) {
        ContentValues values = new ContentValues();
        values.put(MobilizrContract.ProductTable.COLUMN_ID, product.getId());
        values.put(MobilizrContract.ProductTable.COLUMN_NAME, product.getName());
        values.put(MobilizrContract.ProductTable.COLUMN_DESCRIPTION, product.getDescription());
        if (product.getBase64EncodedImageStr() != null)
            values.put(MobilizrContract.ProductTable.COLUMN_ENCODED_IMAGE, product.getBase64EncodedImageStr());
        else
            values.put(MobilizrContract.ProductTable.COLUMN_ENCODED_IMAGE, "");


        return addOrUpdateType(Product.class.getCanonicalName(), values, MobilizrContract.ProductTable.TABLE_NAME);
    }

    /**
     * Used for:
     * - inserting a new wishlist item
     * - increasing/decreasing quantity of an item
     * - quantity of an item
     * - buying an item (marking it as "bought")
     *
     * @param wishlistModel             WishlistModel instance with its values pre-set.
     *                                  <b>Only used when mode is NOT {@link QUANTITY_HANDLING_MODE#ADD_OR_INCREMENT}.</b>
     * @param product                   The product to be persisted and added to the wishlist.
     * @param shopHasProductAggregated  Contains the shop offering the product and the price.
     * @param mode                      {@link QUANTITY_HANDLING_MODE}
     * @return                          True on success, false otherwise.
     */
    public boolean addOrUpdateWishlist(WishlistModel wishlistModel, Product product,
                                    ShopHasProductAggregated shopHasProductAggregated,
                                    QUANTITY_HANDLING_MODE mode) {
        Shop shop = shopHasProductAggregated.getShop();
        ShopHasProduct shopHasProduct = shopHasProductAggregated.getShopHasProduct();
        long quantity = -1, wishlist_id = -1;
        boolean wasSuccessful = false;

        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();

        ContentValues values = new ContentValues();
        Cursor c = null;
        switch (mode) {
            case ADD_OR_INCREMENT: // Coming from ProductView (ADD) and Wishlist view (INCREMENT).
                if (!addOrUpdateShop(shop)) {
                    break;
                }
                if (!addOrUpdateProduct(product)) {
                    break;
                }
                // Add (=1) or Update (+1) => check if it exists
                try {
                    c = db.rawQuery("SELECT " + MobilizrContract.WishlistTable.COLUMN_ID + ", "
                                    + MobilizrContract.WishlistTable.COLUMN_QUANTITY
                                    + " FROM " + MobilizrContract.WishlistTable.TABLE_NAME
                                    + " WHERE " + MobilizrContract.WishlistTable.COLUMN_SHOP_ID_FK
                                    + " = ? AND " + MobilizrContract.WishlistTable.COLUMN_PRODUCT_ID_FK
                                    + " = ? ;",
                            new String[]{shop.getId().toString(), product.getId().toString()});
                    if (c.getCount() > 0) {
                        c.moveToFirst();
                        wishlist_id = c.getInt(c.getColumnIndex(MobilizrContract.WishlistTable.COLUMN_ID));
                        quantity = c.getInt(
                                c.getColumnIndex(MobilizrContract.WishlistTable.COLUMN_QUANTITY));
                    }
                } finally {
                    if (c != null && !c.isClosed())
                        c.close();
                }
                if (quantity == -1) {
                    quantity = 1;
                }
                else {
                    quantity++;
                    values.put(MobilizrContract.WishlistTable.COLUMN_ID, wishlist_id);
                }
                values.put(MobilizrContract.WishlistTable.COLUMN_PRODUCT_ID_FK, product.getId());
                values.put(MobilizrContract.WishlistTable.COLUMN_SHOP_ID_FK, shop.getId());
                values.put(MobilizrContract.WishlistTable.COLUMN_PRICE, shopHasProduct.getPrice());
                values.put(MobilizrContract.WishlistTable.COLUMN_QUANTITY, quantity);
                values.put(MobilizrContract.WishlistTable.COLUMN_WAS_BOUGHT, (byte) 0);
                db.insertWithOnConflict(MobilizrContract.WishlistTable.TABLE_NAME, null, values,
                        SQLiteDatabase.CONFLICT_REPLACE);
                db.setTransactionSuccessful();
                wasSuccessful = true;
                break;

            case SET_BOUGHT:    // Coming from Wishlist view only.
                final String WISHLIST_SET_BOUGHT = "UPDATE "
                        + MobilizrContract.WishlistTable.TABLE_NAME + " SET "
                        + MobilizrContract.WishlistTable.COLUMN_WAS_BOUGHT + " = ?, "
                        + MobilizrContract.WishlistTable.COLUMN_PRICE + " = ? "
                        + "WHERE " + MobilizrContract.WishlistTable.COLUMN_ID + " = ?";
                db.execSQL(WISHLIST_SET_BOUGHT,
                        new String[]{
                                (wishlistModel.getWasBought() ? 1 : 0) + "",
                                wishlistModel.getPrice() + "",
                                wishlistModel.getId().toString()});
                db.setTransactionSuccessful();
                wasSuccessful = true;
                break;

            case DECREMENT:     // Coming from Wishlist view only.
                quantity = wishlistModel.getQuantity() - 1;
                if (quantity >= 1) {
                    final String WISHLIST_DECREMENT_QUANTITY = "UPDATE "
                            + MobilizrContract.WishlistTable.TABLE_NAME + " SET "
                            + MobilizrContract.WishlistTable.COLUMN_QUANTITY + " = ? " +
                            "WHERE " + MobilizrContract.WishlistTable.COLUMN_ID + " = ?";
                    db.execSQL(WISHLIST_DECREMENT_QUANTITY,
                            new String[]{Long.toString(quantity), wishlistModel.getId().toString()});
                    db.setTransactionSuccessful();
                    wasSuccessful = true;
                }
                else {
                    // Simply remove wishlist item.
                    wasSuccessful = deleteWishlistEntry(wishlistModel);
                }
                break;
        }
        db.endTransaction();
        return wasSuccessful;
    }

    public boolean deleteWishlistEntry(WishlistModel wishlistModel) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();

        int numRows = db.delete(MobilizrContract.WishlistTable.TABLE_NAME, String.format(
                        MobilizrContract.WishlistTable.COLUMN_ID + " = %s",
                        wishlistModel.getId().toString()), null);

        if (numRows == 1)
            db.setTransactionSuccessful();
        db.endTransaction();
        return numRows == 1;
    }

    /**
     * Queries wishlist, shop and product tables and combines them to a List of WishlistBundles.
     * @return      The list representing the entries in the wishlist table
     *              <WishlistModel, Shop, Product> on success, or null on failure.
     */
    public List<WishlistBundle> getAllWishlistEntries() {
           final String WISHLIST_SELECT_QUERY = String.format("SELECT %s.%s, %s, %s, %s, %s, %s, " +  // Wishlist table columns
                   "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, " + // Shop table columns (excluding id)
                   "%s, %s, %s  " +
                   "FROM %s " +
                   "INNER JOIN %s ON %s.%s = %s.%s " +
                   "INNER JOIN %s ON %s.%s = %s.%s",
                   // Wishlist table columns
                   MobilizrContract.WishlistTable.TABLE_NAME, MobilizrContract.WishlistTable.COLUMN_ID,
                   MobilizrContract.WishlistTable.COLUMN_SHOP_ID_FK, MobilizrContract.WishlistTable.COLUMN_PRODUCT_ID_FK,
                   MobilizrContract.WishlistTable.COLUMN_PRICE, MobilizrContract.WishlistTable.COLUMN_QUANTITY,
                   MobilizrContract.WishlistTable.COLUMN_WAS_BOUGHT,

                   // Shop table columns
                   MobilizrContract.ShopTable.COLUMN_BRAND, MobilizrContract.ShopTable.COLUMN_LAT,
                   MobilizrContract.ShopTable.COLUMN_LON, MobilizrContract.ShopTable.COLUMN_ADDRESS,
                   MobilizrContract.ShopTable.COLUMN_CITY, MobilizrContract.ShopTable.COLUMN_POSTAL_CODE,
                   MobilizrContract.ShopTable.COLUMN_PHONE, MobilizrContract.ShopTable.COLUMN_MOBILE_PHONE,
                   MobilizrContract.ShopTable.COLUMN_TRN, MobilizrContract.ShopTable.COLUMN_RATING_SUM,
                   MobilizrContract.ShopTable.COLUMN_RATING_COUNT,

                   // Product table columns
                   MobilizrContract.ProductTable.COLUMN_NAME, MobilizrContract.ProductTable.COLUMN_DESCRIPTION,
                   MobilizrContract.ProductTable.COLUMN_ENCODED_IMAGE,

                   // FROM .. INNER JOIN ..
                   MobilizrContract.WishlistTable.TABLE_NAME,
                   MobilizrContract.ShopTable.TABLE_NAME,
                   MobilizrContract.WishlistTable.TABLE_NAME, MobilizrContract.WishlistTable.COLUMN_SHOP_ID_FK,
                   MobilizrContract.ShopTable.TABLE_NAME, MobilizrContract.ShopTable.COLUMN_ID,

                   MobilizrContract.ProductTable.TABLE_NAME,
                   MobilizrContract.WishlistTable.TABLE_NAME, MobilizrContract.WishlistTable.COLUMN_PRODUCT_ID_FK,
                   MobilizrContract.ProductTable.TABLE_NAME, MobilizrContract.ProductTable.COLUMN_ID
           );

        List<WishlistBundle> wishlistBundleList = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(WISHLIST_SELECT_QUERY, null);
        try {
            if (c.moveToFirst()) {
                do {
                    long shopId = c.getLong(c.getColumnIndex(MobilizrContract.WishlistTable.COLUMN_SHOP_ID_FK));
                    long productId = c.getLong(c.getColumnIndex(MobilizrContract.WishlistTable.COLUMN_PRODUCT_ID_FK));
                    WishlistBundle bundle = new WishlistBundle();
                    bundle.setShop(extractShop(c, shopId));
                    bundle.setProduct(extractProduct(c, productId));
                    bundle.setWishlistModel(extractWishlistModel(c));
                    wishlistBundleList.add(bundle);
                } while (c.moveToNext());
            }
        } catch (Exception e) {
            Timber.e(e, "Failure getting all wishlist entries.");
            wishlistBundleList = null;
        }
        finally {
            if (c != null && !c.isClosed())
                c.close();
        }
        return wishlistBundleList;
    }

    public WishlistBundle getWishlistBundle(long wishlistTableId) {
        final String WISHLIST_SELECT_QUERY = String.format("SELECT %s.%s, %s, %s, %s, %s, %s, " +  // Wishlist table columns
                        "%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, " + // Shop table columns (excluding id)
                        "%s, %s, %s  " +
                        "FROM %s " +
                        "INNER JOIN %s ON %s.%s = %s.%s " +
                        "INNER JOIN %s ON %s.%s = %s.%s " +
                        "WHERE %s.%s = %s",
                // Wishlist table columns
                MobilizrContract.WishlistTable.TABLE_NAME, MobilizrContract.WishlistTable.COLUMN_ID,
                MobilizrContract.WishlistTable.COLUMN_SHOP_ID_FK, MobilizrContract.WishlistTable.COLUMN_PRODUCT_ID_FK,
                MobilizrContract.WishlistTable.COLUMN_PRICE, MobilizrContract.WishlistTable.COLUMN_QUANTITY,
                MobilizrContract.WishlistTable.COLUMN_WAS_BOUGHT,

                // Shop table columns
                MobilizrContract.ShopTable.COLUMN_BRAND, MobilizrContract.ShopTable.COLUMN_LAT,
                MobilizrContract.ShopTable.COLUMN_LON, MobilizrContract.ShopTable.COLUMN_ADDRESS,
                MobilizrContract.ShopTable.COLUMN_CITY, MobilizrContract.ShopTable.COLUMN_POSTAL_CODE,
                MobilizrContract.ShopTable.COLUMN_PHONE, MobilizrContract.ShopTable.COLUMN_MOBILE_PHONE,
                MobilizrContract.ShopTable.COLUMN_TRN, MobilizrContract.ShopTable.COLUMN_RATING_SUM,
                MobilizrContract.ShopTable.COLUMN_RATING_COUNT,

                // Product table columns
                MobilizrContract.ProductTable.COLUMN_NAME, MobilizrContract.ProductTable.COLUMN_DESCRIPTION,
                MobilizrContract.ProductTable.COLUMN_ENCODED_IMAGE,

                // FROM .. INNER JOIN ..
                MobilizrContract.WishlistTable.TABLE_NAME,
                MobilizrContract.ShopTable.TABLE_NAME,
                MobilizrContract.WishlistTable.TABLE_NAME, MobilizrContract.WishlistTable.COLUMN_SHOP_ID_FK,
                MobilizrContract.ShopTable.TABLE_NAME, MobilizrContract.ShopTable.COLUMN_ID,

                MobilizrContract.ProductTable.TABLE_NAME,
                MobilizrContract.WishlistTable.TABLE_NAME, MobilizrContract.WishlistTable.COLUMN_PRODUCT_ID_FK,
                MobilizrContract.ProductTable.TABLE_NAME, MobilizrContract.ProductTable.COLUMN_ID,

                MobilizrContract.WishlistTable.TABLE_NAME, MobilizrContract.WishlistTable.COLUMN_ID,
                Long.toString(wishlistTableId)
        );

        WishlistBundle wishlistBundle = null;
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(WISHLIST_SELECT_QUERY, null);
        try {
            if (c.moveToFirst()) {
                wishlistBundle = new WishlistBundle();
                long shopId = c.getLong(c.getColumnIndex(MobilizrContract.WishlistTable.COLUMN_SHOP_ID_FK));
                long productId = c.getLong(c.getColumnIndex(MobilizrContract.WishlistTable.COLUMN_PRODUCT_ID_FK));
                wishlistBundle.setShop(extractShop(c, shopId));
                wishlistBundle.setProduct(extractProduct(c, productId));
                wishlistBundle.setWishlistModel(extractWishlistModel(c));
            }
        } catch (Exception e) {
            Timber.e(e, "Failure getting all wishlist entries.");
            wishlistBundle = null;
        }
        finally {
            if (c != null && !c.isClosed())
                c.close();
        }
        return wishlistBundle;
    }

    /**
     * Describes the intent of addOrUpdateWishlist method.
     */
    public enum QUANTITY_HANDLING_MODE {
        /**
         * Used when addOrUpdateWishlist is called from Wishlist view (clicking on minus button).
         */
        DECREMENT,
        /**
         * Used when addOrUpdateWishlist is called from Wishlist view (clicking on "buy" button).
         */
        SET_BOUGHT,
        /**
         * Either from ProductView when "Adding to wishlist"
         * or WishlistView when clicking the plus button.
         */
        ADD_OR_INCREMENT
    }

    // Helpers -------------------------------------------------------------------------------------
    private static Shop extractShop(Cursor c, long shopId) {
        Shop s = new Shop();
        s.setId(shopId);
        s.setBrand(c.getString(c.getColumnIndex(MobilizrContract.ShopTable.COLUMN_BRAND)));
        s.setAddress(c.getString(c.getColumnIndex(MobilizrContract.ShopTable.COLUMN_ADDRESS)));
        s.setLat(c.getDouble(c.getColumnIndex(MobilizrContract.ShopTable.COLUMN_LAT)));
        s.setLon(c.getDouble(c.getColumnIndex(MobilizrContract.ShopTable.COLUMN_LON)));
        s.setCity(c.getString(c.getColumnIndex(MobilizrContract.ShopTable.COLUMN_CITY)));
        s.setPostalCode(c.getString(c.getColumnIndex(MobilizrContract.ShopTable.COLUMN_POSTAL_CODE)));
        s.setPhone(c.getString(c.getColumnIndex(MobilizrContract.ShopTable.COLUMN_PHONE)));
        s.setMobilePhone(c.getString(c.getColumnIndex(MobilizrContract.ShopTable.COLUMN_MOBILE_PHONE)));
        s.setTrn(c.getString(c.getColumnIndex(MobilizrContract.ShopTable.COLUMN_TRN)));
        s.setRatingSum(c.getLong(c.getColumnIndex(MobilizrContract.ShopTable.COLUMN_RATING_SUM)));
        s.setRatingCount(c.getLong(c.getColumnIndex(MobilizrContract.ShopTable.COLUMN_RATING_COUNT)));
        return s;
    }

    private static Product extractProduct(Cursor c, long productId) {
        Product p = new Product();
        p.setId(productId);
        p.setName(c.getString(c.getColumnIndex(MobilizrContract.ProductTable.COLUMN_NAME)));
        p.setDescription(c.getString(c.getColumnIndex(MobilizrContract.ProductTable.COLUMN_DESCRIPTION)));
        p.setBase64EncodedImageStr(c.getString(c.getColumnIndex(MobilizrContract.ProductTable.COLUMN_ENCODED_IMAGE)));
        return p;
    }

    private static WishlistModel extractWishlistModel(Cursor c) {
        WishlistModel w = new WishlistModel();
        w.setId(c.getLong(c.getColumnIndex(MobilizrContract.WishlistTable.COLUMN_ID)));
        w.setShopId(c.getLong(c.getColumnIndex(MobilizrContract.WishlistTable.COLUMN_SHOP_ID_FK)));
        w.setProductId(c.getLong(c.getColumnIndex(MobilizrContract.WishlistTable.COLUMN_PRODUCT_ID_FK)));
        w.setPrice(c.getDouble(c.getColumnIndex(MobilizrContract.WishlistTable.COLUMN_PRICE)));
        w.setQuantity(c.getLong(c.getColumnIndex(MobilizrContract.WishlistTable.COLUMN_QUANTITY)));
        w.setWasBought(c.getInt(c.getColumnIndex(MobilizrContract.WishlistTable.COLUMN_WAS_BOUGHT)) == 1);
        return  w;
    }

    public ArrayList<Cursor> getData(String Query){
        //get writable database
        SQLiteDatabase sqlDB = this.getWritableDatabase();
        String[] columns = new String[] { "mesage" };
        //an array list of cursor to save two cursors one has results from the query
        //other cursor stores error message if any errors are triggered
        ArrayList<Cursor> alc = new ArrayList<Cursor>(2);
        MatrixCursor Cursor2= new MatrixCursor(columns);
        alc.add(null);
        alc.add(null);


        try{
            String maxQuery = Query ;
            //execute the query results will be save in Cursor c
            Cursor c = sqlDB.rawQuery(maxQuery, null);


            //add value to cursor2
            Cursor2.addRow(new Object[] { "Success" });

            alc.set(1,Cursor2);
            if (null != c && c.getCount() > 0) {


                alc.set(0,c);
                c.moveToFirst();

                return alc ;
            }
            return alc;
        } catch(SQLException sqlEx){
            Log.d("printing exception", sqlEx.getMessage());
            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+sqlEx.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        } catch(Exception ex){

            Log.d("printing exception", ex.getMessage());

            //if any exceptions are triggered save the error message to cursor an return the arraylist
            Cursor2.addRow(new Object[] { ""+ex.getMessage() });
            alc.set(1,Cursor2);
            return alc;
        }
    }
}














