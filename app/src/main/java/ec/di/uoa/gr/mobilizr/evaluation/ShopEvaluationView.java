package ec.di.uoa.gr.mobilizr.evaluation;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ec.di.uoa.gr.mobilizr.MobilizrApplication;
import ec.di.uoa.gr.mobilizr.R;
import ec.di.uoa.gr.mobilizr.data.models.WishlistBundle;
import ec.di.uoa.gr.mobilizr.shops.FeedbackAsyncTask;
import ec.di.uoa.gr.mobilizr.util.UiUtils;

public class ShopEvaluationView extends Fragment
        implements FeedbackAsyncTask.OnFeedbackOutcomeReadyListener{
    @BindView(R.id.shop_eval_real_price_checkbox)
    CheckBox shopEvalRealPriceCheckbox;
    @BindView(R.id.shop_eval_real_price_label)
    TextView shopEvalRealPriceLabel;
    @BindView(R.id.shop_eval_real_price_edit_text)
    EditText shopEvalRealPriceEditText;
    @BindView(R.id.shop_eval_real_price_euro_sign)
    TextView shopEvalRealPriceEuroSign;
    @BindView(R.id.shop_eval_rating_bar)
    RatingBar shopEvalRatingBar;
    @BindView(R.id.shop_eval_comment_message)
    EditText shopEvalCommentMessage;
    @BindView(R.id.shop_eval_submit_button)
    Button shopEvalSubmitButton;
    private WishlistBundle mWishlistBundle;
    private View mRootView;


    public ShopEvaluationView() {
        //Required empty contractor
    }

    public static ShopEvaluationView newInstance(String wishlistItemIdForReviewKey,
                                                 long wishlistItemId) {
        ShopEvaluationView fragment = new ShopEvaluationView();
        Bundle args = new Bundle();
        args.putLong(wishlistItemIdForReviewKey, wishlistItemId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            long wishlistItemId = (long) getArguments().get(getString(R.string.wishlist_item_id_for_review_key));
            mWishlistBundle = MobilizrApplication.getDbHelper().getWishlistBundle(wishlistItemId);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(R.layout.shop_evaluation_view, container, false);
        ButterKnife.bind(this, mRootView);

        // TODO Title isn't shown
        MobilizrApplication.getsActionBar().setTitle(R.string.toolbar_title_shop_eval);
        MobilizrApplication.getsActionBar().setDisplayHomeAsUpEnabled(true);
        // TODO Check back button

        toggleRealPriceUI();
        return mRootView;
    }

    @OnClick({R.id.shop_eval_real_price_checkbox, R.id.shop_eval_submit_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.shop_eval_real_price_checkbox:
                toggleRealPriceUI();
                break;
            case R.id.shop_eval_submit_button:
                onSubmitButtonClicked();
                break;
        }
    }

    @Override
    public void onOutcomeReady(Boolean wasSuccessful) {
        // Inform the user the outcome.
        if (wasSuccessful == null || !wasSuccessful) {
            UiUtils.snackBarNotify(mRootView, getString(R.string.feedback_submission_error_msg));
        }
        else {
            showToast(Toast.LENGTH_LONG, R.layout.success_toast, R.string.feedback_successful_msg);
            Intent intent = new Intent(getContext(), getActivity().getClass());
            getActivity().finish();
            startActivity(intent);
        }
    }

    // Helpers -------------------------------------------------------------------------------------
    private void toggleRealPriceUI() {
        if (shopEvalRealPriceCheckbox.isChecked()) {
            shopEvalRealPriceLabel.setVisibility(View.VISIBLE);
            shopEvalRealPriceEuroSign.setVisibility(View.VISIBLE);
            shopEvalRealPriceEditText.setVisibility(View.VISIBLE);
        } else {
            shopEvalRealPriceLabel.setVisibility(View.GONE);
            shopEvalRealPriceEuroSign.setVisibility(View.GONE);
            shopEvalRealPriceEditText.setVisibility(View.GONE);

        }
    }

    private void onSubmitButtonClicked() {
        double rating = shopEvalRatingBar.getRating();
        double realPrice = mWishlistBundle.getWishlistModel().getPrice();
        String comment = "";

        // Validate real price field.
        if (shopEvalRealPriceCheckbox.isChecked()) {
            // Penalty rating if not actual price.
            if (!shopEvalRealPriceEditText.getText().toString().isEmpty()) {
                if (!shopEvalRealPriceEditText.getText().toString().equals(mWishlistBundle.getWishlistModel().getPrice().toString())) {
                    rating = rating - MobilizrApplication.NON_REAL_PRICE_RATING_PENALTY;
                    realPrice = Double.parseDouble(shopEvalRealPriceEditText.getText().toString());
                } else {
                    UiUtils.snackBarNotify(mRootView, "If the actual price you've paid is same to the initial don't check the checkbox.");
                    toggleRealPriceUI();
                    return;
                }
            } else {
                UiUtils.snackBarNotify(mRootView, "You should fill in the actual price you've paid.");
                return;
            }
        }

        if (!shopEvalCommentMessage.getText().toString().isEmpty()) {
            comment = shopEvalCommentMessage.getText().toString();
        } else {
            UiUtils.snackBarNotify(mRootView, "Do leave a comment now, be responsible.");
            return;
        }

        if (realPrice == mWishlistBundle.getWishlistModel().getPrice())
            realPrice = -1;
        new FeedbackAsyncTask(
                this,
                mWishlistBundle,
                comment,
                (long)rating,
                realPrice).execute();
    }
    /**
     * Displays a toast message defined in the custom toastLayoutId layout.
     * @param toastDuration Obtained from Toast.LENGH_{SHORT, LONG}
     * @param toastLayoutId The id of the custom toast layout (R.layout.X)
     */
    private void showToast(int toastDuration, int toastLayoutId, int strMessageID) {
        Toast t = Toast.makeText(getContext(), "", toastDuration);
        // Load custom toast view
        View view = LayoutInflater.from(getContext()).inflate(
                getContext().getResources().getLayout(toastLayoutId), null);
        ((TextView)view).setText(strMessageID);
        t.setView(view);
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        double toastHeight = displayMetrics.heightPixels * 0.05;
        t.setGravity(Gravity.BOTTOM | Gravity.CENTER_VERTICAL, 0, (int) toastHeight);
        t.show();
        t.show(); // Purposely so that the toast lasts longer
    }
}
