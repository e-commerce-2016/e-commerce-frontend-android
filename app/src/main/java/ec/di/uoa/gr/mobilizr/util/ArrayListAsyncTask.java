package ec.di.uoa.gr.mobilizr.util;


import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import ec.di.uoa.gr.mobilizr.MobilizrApplication;
import ec.di.uoa.gr.mobilizr.R;
import timber.log.Timber;

/**
 * Generic extension of AsyncTask.
 * @param <Params> Same as AsyncTask.
 * @param <Progress> Same as AsyncTask.
 * @param <ContainedType> The type contained into the ArrayList container that will be returned.
 */
public abstract class ArrayListAsyncTask<Params, Progress, ContainedType>
        extends AsyncTask<Params, Progress, ArrayList<ContainedType>> {
    // Members -------------------------------------------------------------------------------------
    private OnArrayListAsyncTaskCompletedListener<ContainedType> mListener = null;
    private JSONUnMarshaller<ContainedType> mUnMarshaller = null;
    private ProgressDialog mDialog;

    // ---------------------------------------------------------------------------------------------
    /**
     * @param activityContext   The context of the activity in which the fragment that initiated
     *                          the task resides.
     * @param listener  Interface instance whose method will be called at OnPostExecute to handle
     *                  the populated ArrayList<ContainedType>.
     * @param unMarshaller  JSON UnMarshaller method for receiving a JSONObject and converting it
     *                      to type T.
     */
    public ArrayListAsyncTask(
            Context activityContext,
            OnArrayListAsyncTaskCompletedListener<ContainedType> listener,
                              JSONUnMarshaller<ContainedType> unMarshaller) {
        this.mListener = listener;
        this.mUnMarshaller = unMarshaller;
        this.mDialog = new ProgressDialog(activityContext);
    }

    /**
     * Receives the JSON String reply <b>(with the assumption that all replies from our backend
     * are in the format of JSONArray that contains multiple JSON Objects)</b>.
     * @param jsonStr   The JSON String reply from the backend.
     * @return  ArrayList<ContainedType> using JSONUnMarshaller interface.
     * @throws JSONException
     */
    protected ArrayList<ContainedType> unMarshalJSONtoArrayList(String jsonStr) throws JSONException {
        ArrayList<ContainedType> list = new ArrayList<>();
        JSONArray JSONArray = new JSONArray(jsonStr);

        for (int i = 0; i < JSONArray.length(); i++) {
            list.add(mUnMarshaller.unMarshallJSON((JSONObject) JSONArray.get(i)));
        }

        Timber.i("Created an ArrayList of size(%d): %s",
                list.size(), list.toString());
        return list;
    }

    // AsyncTask methods ---------------------------------------------------------------------------
    @Override
    protected void onPreExecute() {
        mDialog.setMessage(MobilizrApplication.getContext().getString(R.string.please_wait_msg));
        mDialog.show();
    }

    /**
     * Expects params to contain only one parameter, which must be of type URI.
     * @param params (of type URI) prebuilt by the client async task.
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    protected ArrayList<ContainedType> doInBackground(Params... params) {
        if (params.length != 1) {
            Timber.e("Erroneous parameter number: Expected 1 and got: %d", params.length);
            return null;
        }

        Uri builtUri = (Uri) params[0];

        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        StringBuffer buffer = null;

        try {
            URL url = new URL(builtUri.toString());
            Timber.v("The URL is: %s", url.toString());

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setConnectTimeout(10000);
            urlConnection.connect();

            // Read response into an inputStream
            InputStream inputStream = urlConnection.getInputStream();
            reader = new BufferedReader(new InputStreamReader(inputStream));

            buffer = new StringBuffer();
            // Pass the response into the buffer appending '\n' for easier debugging
            String line;
            while ((line = reader.readLine()) != null)
                buffer.append(line).append('\n');

            if (buffer.length() == 0)
                // Stream was empty. No point in parsing.
                return null;
        } catch (IOException e) {
            Timber.e("Error: %s", e.toString());
            return null;
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Timber.e("Failure closing reader: %s", e.toString());
                }
            }
        }

        // Using ArrayList and not simply List, as we need a serializable collection in order
        // to pass it as an argument to the new fragment (via Bundle) and ArrayList is serializable.
        ArrayList<ContainedType> arrayList = null;
        try {
            arrayList = unMarshalJSONtoArrayList(buffer.toString());
        } catch (JSONException e) {
            Timber.e("Failure parsing ProductsJson response: %s", e.getMessage());
        }
        return arrayList;
    }

    @Override
    protected void onPostExecute(ArrayList<ContainedType> containedTypes) {
        if (mDialog.isShowing())
            mDialog.dismiss();

        if (mListener != null)
            mListener.onAsyncTaskCompleted(containedTypes);
    }
}
