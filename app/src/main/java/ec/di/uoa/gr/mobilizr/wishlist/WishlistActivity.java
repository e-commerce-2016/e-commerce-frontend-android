package ec.di.uoa.gr.mobilizr.wishlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import ec.di.uoa.gr.mobilizr.MainActivity;
import ec.di.uoa.gr.mobilizr.R;
import ec.di.uoa.gr.mobilizr.evaluation.ShopEvaluationView;

public class WishlistActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);

        // Ensure activity is using the fragment_main_container layout
        if (findViewById(R.id.wishlist_content_main) != null) {

            // If we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            Intent intent = getIntent();
            long wishlistItemId = intent.getLongExtra(getString(R.string.wishlist_item_id_for_review_key), -1);
            if (wishlistItemId != -1) {
                // Intent received from StreetView activity.
                // Forward wishlistItemId to ShopEvaluationView.
                ShopEvaluationView shopEvaluationView =
                        ShopEvaluationView.newInstance(getString(R.string.wishlist_item_id_for_review_key), wishlistItemId);
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.setCustomAnimations(R.anim.pop_enter, R.anim.pop_exit)
                        .add(R.id.wishlist_content_main, shopEvaluationView).commit();
            } else {
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                transaction.setCustomAnimations(R.anim.pop_enter, R.anim.pop_exit)
                        .add(R.id.wishlist_content_main, new WishlistView()).commit();
            }
        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
    }
}
