package ec.di.uoa.gr.mobilizr.wishlist;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ec.di.uoa.gr.mobilizr.MainActivity;
import ec.di.uoa.gr.mobilizr.MobilizrApplication;
import ec.di.uoa.gr.mobilizr.R;
import ec.di.uoa.gr.mobilizr.data.models.WishlistBundle;

public class WishlistView extends Fragment {
    @BindView(R.id.wishlist_info_cost)
    TextView wishlistInfoCost;
    @BindView(R.id.wishlist_info_total_products)
    TextView wishlistInfoTotalProducts;
    @BindView(R.id.wishlist_product_shop_listview)
    ListView wishlistProductShopListview;
    @BindView(R.id.wishlist_view_no_content_go_to_search_btn)
    ImageButton wishlistViewNoContentButton;
    @BindView(R.id.wishlist_view_no_content_layout)
    RelativeLayout wishlistViewNoContentLayout;
    @BindView(R.id.wishlist_view_has_content_layout)
    RelativeLayout wishlistViewHasContentLayout;

    private List<WishlistBundle> mWishlistBundleList;
    private WishlistArrayAdapter mWishlistArrayAdapter;

    public WishlistView() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MobilizrApplication.getsActionBar().setTitle(R.string.toolbar_title_wishlist);
        MobilizrApplication.getsActionBar().setDisplayHomeAsUpEnabled(true);

        mWishlistBundleList = MobilizrApplication.getDbHelper().getAllWishlistEntries();
        mWishlistArrayAdapter = new WishlistArrayAdapter(this, mWishlistBundleList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        RelativeLayout wishlistRootView = (RelativeLayout) inflater.inflate(R.layout.wishlist_view, container, false);
        ButterKnife.bind(this, wishlistRootView);

        if (mWishlistBundleList.size() != 0) {
            wishlistViewNoContentLayout.setVisibility(View.GONE);
            wishlistViewHasContentLayout.setVisibility(View.VISIBLE);
        }

        updateTotalCostAndQuantity();

        wishlistViewNoContentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
            }
        });

        wishlistProductShopListview.setAdapter(mWishlistArrayAdapter);
        return wishlistRootView;
    }

    public void updateTotalCostAndQuantity() {
        double totalCost = 0.0;
        int quantity = 0;
        for (WishlistBundle bundle : mWishlistBundleList) {
            totalCost += bundle.getWishlistModel().getQuantity() * bundle.getWishlistModel().getPrice();
            quantity += bundle.getWishlistModel().getQuantity();
        }
        String costStr = Math.round(totalCost) + " €";
        wishlistInfoCost.setText(costStr);
        String quantityStr = quantity + "";
        wishlistInfoTotalProducts.setText(quantityStr);
    }
}
