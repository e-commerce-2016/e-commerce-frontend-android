package ec.di.uoa.gr.mobilizr.shops.entities;

import java.io.Serializable;

public class ShopHasProduct implements Serializable {
    private static final long serialVersionUID = 1460633651669166437L;

    private Long id;
    private Long shopId;
    private Long productsId;
    private Double price;
    private Long quantity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getShopId() {
        return shopId;
    }

    public void setShopId(Long shop_id) {
        this.shopId = shop_id;
    }

    public Long getProductsId() {
        return productsId;
    }

    public void setProductId(Long products_id) {
        this.productsId = products_id;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "ShopHasProduct[id=" + id + " - price: " + price + "]";
    }
}