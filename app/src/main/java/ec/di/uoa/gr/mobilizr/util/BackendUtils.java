package ec.di.uoa.gr.mobilizr.util;

import android.content.Context;
import android.support.annotation.NonNull;

import ec.di.uoa.gr.mobilizr.R;

public class BackendUtils {
    /**
     * Retrieves the endPoint and builds the backend endpoint URL
     * @param ctx
     * @param endPoint Loaded using web_services:webServicesEntrypoints string array.
     * @return The URL <b>without</b> and query parameters and other extensions.
     */
    public static String prepareEndpointURL(Context ctx, String endPoint) {
        return ctx.getResources().getString(R.string.http)
                + "://" + getBackendIP(ctx) + endPoint;
    }

    @NonNull
    public static String getBackendIP(Context ctx) {
        return ctx.getResources().getString(R.string.ServerIP);
    }
}
