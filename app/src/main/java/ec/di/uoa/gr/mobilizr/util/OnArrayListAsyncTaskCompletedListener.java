package ec.di.uoa.gr.mobilizr.util;

import java.util.ArrayList;


/**
 * Interface that delegates the onPostExecute event to a delegate object.
 * <b>Combined with the ArrayListAsyncTask.</b>
 * @param <T> The type parameter of ArrayList container (a.k.a. "contained type" of the result).
 */
public interface OnArrayListAsyncTaskCompletedListener<T> {
    /**
     * Callback method that will be called at OnPostExecute method of the ArrayListAsyncTask.
     * @param resultsArrayList The ArrayList<T> container with the results.
     */
    void onAsyncTaskCompleted(ArrayList<T> resultsArrayList);
}
