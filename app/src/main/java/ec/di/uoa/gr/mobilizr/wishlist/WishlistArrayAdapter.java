package ec.di.uoa.gr.mobilizr.wishlist;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ec.di.uoa.gr.mobilizr.MapActivity;
import ec.di.uoa.gr.mobilizr.MobilizrApplication;
import ec.di.uoa.gr.mobilizr.R;
import ec.di.uoa.gr.mobilizr.data.MobilizrDbHelper;
import ec.di.uoa.gr.mobilizr.data.models.WishlistBundle;
import ec.di.uoa.gr.mobilizr.shops.entities.ShopHasProduct;
import ec.di.uoa.gr.mobilizr.shops.entities.ShopHasProductAggregated;
import ec.di.uoa.gr.mobilizr.util.UiUtils;

public class WishlistArrayAdapter extends ArrayAdapter<WishlistBundle> {
    private List<WishlistBundle> mWishlistBundleList;
    private WishlistView mWishlistView;
    private Context mContext;

    public WishlistArrayAdapter(WishlistView wishlistView, List<WishlistBundle> objects) {
        super(wishlistView.getContext(), 0, objects);
        mWishlistBundleList = objects;
        mWishlistView = wishlistView;
        mContext = wishlistView.getContext();
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final WishlistBundle bundle = getItem(position);
        final ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.wishlist_view_list_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        // Product related
        String quantityStr = bundle.getWishlistModel().getQuantity().toString() + " x ";
        holder.wishlistProductShopListItemProductQuantity.setText(quantityStr);
        holder.wishlistProductShopListItemProduct.setText(bundle.getProduct().getName());
        String priceStr = Math.round(bundle.getWishlistModel().getPrice() * 100) / 100 + " €";
        holder.wishlistProductShopListItemProductPrice.setText(priceStr);

        // Shop related
        holder.wishlistProductShopListItemShopBrand.setText(bundle.getShop().getBrand());
        String shopLocationStr =
                bundle.getShop().getAddress() != null ? bundle.getShop().getAddress() : "";
        if (bundle.getShop().getCity() != null)
            shopLocationStr += (shopLocationStr.isEmpty() ? "" : ", ") + bundle.getShop().getCity();
        if (bundle.getShop().getPostalCode() != null)
            shopLocationStr += (shopLocationStr.isEmpty() ? "" : ", ") + bundle.getShop().getPostalCode();
        holder.wishlistProductShopListItemShopLocation.setText(shopLocationStr);

        String phoneStr = bundle.getShop().getPhone(), mobileStr = bundle.getShop().getMobilePhone();
        if (phoneStr != null || mobileStr != null) {
            String contactDetails = (phoneStr != null) ? phoneStr : "";
            if (mobileStr != null) {
                if (!contactDetails.isEmpty())
                    contactDetails += ", ";
                contactDetails += mobileStr;
            }
            holder.wishlistProductShopListItemShopContactDetails.setText(contactDetails);
        } else
            holder.wishlistProductShopListItemShopContactDetailsLabel.setVisibility(View.GONE);

        // Increase / Decrease quantity handlers ---------------------------------------------------
        holder.decreaseQuantityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleQuantity(bundle, MobilizrDbHelper.QUANTITY_HANDLING_MODE.DECREMENT);
                mWishlistView.updateTotalCostAndQuantity();
                notifyDataSetChanged();
            }
        });

        holder.increaseQuantityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleQuantity(bundle, MobilizrDbHelper.QUANTITY_HANDLING_MODE.ADD_OR_INCREMENT);
                mWishlistView.updateTotalCostAndQuantity();
                notifyDataSetChanged();
            }
        });

        if (bundle.getWishlistModel().getWasBought()) {
            prepareBoughtItem(bundle, holder);
        }

        holder.goToMapActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), MapActivity.class);
                intent.putExtra(getContext()
                                .getString(R.string.wishlist_item_id_for_review_key),
                        getItem(position).getWishlistModel().getId());
                intent.putExtra(getContext().getString(R.string.wishlist_item_shop_latlng_key),
                        new LatLng(
                                getItem(position).getShop().getLat(),
                                getItem(position).getShop().getLon()));
                intent.putExtra(getContext().getString(R.string.wishlist_item_shop_name_key),
                        getItem(position).getShop().getBrand());

                String shopInfoStr = getItem(position).getShop().getAddress();
                if (getItem(position).getShop().getCity() != null)
                    shopInfoStr += ", " + getItem(position).getShop().getCity();
                if (getItem(position).getShop().getPhone() != null)
                    shopInfoStr += ", " + getItem(position).getShop().getPhone();

                intent.putExtra(getContext().getString(R.string.wishlist_item_shop_info_key),
                        shopInfoStr);
                getContext().startActivity(intent);
            }
        });

        return convertView;
    }

    static class ViewHolder {
        @BindView(R.id.wishlist_view_list_item_layout)
        LinearLayout wishlistViewListItemLayout;
        @BindView(R.id.wishlist_product_shop_list_item_product_quantity)
        TextView wishlistProductShopListItemProductQuantity;
        @BindView(R.id.wishlist_product_shop_list_item_product)
        TextView wishlistProductShopListItemProduct;
        @BindView(R.id.wishlist_product_shop_list_item_product_price)
        TextView wishlistProductShopListItemProductPrice;
        @BindView(R.id.wishlist_product_shop_list_item_header)
        RelativeLayout wishlistProductShopListItemHeader;
        @BindView(R.id.wishlist_product_shop_list_item_shop_brand)
        TextView wishlistProductShopListItemShopBrand;
        @BindView(R.id.wishlist_product_shop_list_item_shop_location)
        TextView wishlistProductShopListItemShopLocation;
        @BindView(R.id.wishlist_product_shop_list_item_shop)
        RelativeLayout wishlistProductShopListItemShop;
        @BindView(R.id.goto_map_activity)
        Button goToMapActivity;
        @BindView(R.id.decrease_quantity_button)
        Button decreaseQuantityButton;
        @BindView(R.id.increase_quantity_button)
        Button increaseQuantityButton;
        @BindView(R.id.handle_quantity_buttons_layout)
        RelativeLayout handleQuantityButtonsLayout;
        @BindView(R.id.handle_quantity_layout)
        LinearLayout handleQuantityLayout;
        @BindView(R.id.wishlist_product_shop_list_item_actions_layout)
        RelativeLayout wishlistProductShopListItemActionsLayout;
        @BindView(R.id.wishlist_product_shop_list_item_body)
        LinearLayout wishlistProductShopListItemBody;
        @BindView(R.id.wishlist_product_shop_list_item_shop_contact_details_label)
        TextView wishlistProductShopListItemShopContactDetailsLabel;
        @BindView(R.id.wishlist_product_shop_list_item_shop_contact_details)
        TextView wishlistProductShopListItemShopContactDetails;
        @BindView(R.id.wishlist_view_List_item_delete_btn)
        Button wishlistViewListItemDeleteBtn;


        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    private void handleQuantity(WishlistBundle bundle,
                                MobilizrDbHelper.QUANTITY_HANDLING_MODE mode) {
        ShopHasProductAggregated shpa = new ShopHasProductAggregated();
        shpa.setShop(bundle.getShop());

        ShopHasProduct shp = new ShopHasProduct();
        shp.setQuantity(bundle.getWishlistModel().getQuantity());
        shp.setPrice(bundle.getWishlistModel().getPrice());
        shp.setShopId(bundle.getShop().getId());
        shp.setProductId(bundle.getProduct().getId());
        shpa.setShopHasProduct(shp);

        boolean wasSuccessful = MobilizrApplication.getDbHelper()
                .addOrUpdateWishlist(bundle.getWishlistModel(),
                        bundle.getProduct(), shpa, mode);

        if (wasSuccessful) {
            long newQuantity = bundle.getWishlistModel().getQuantity();
            if (mode == MobilizrDbHelper.QUANTITY_HANDLING_MODE.ADD_OR_INCREMENT)
                newQuantity++;
            else if (mode == MobilizrDbHelper.QUANTITY_HANDLING_MODE.DECREMENT)
                newQuantity--;

            if (newQuantity != 0)
                bundle.getWishlistModel().setQuantity(newQuantity);
            else
                remove(bundle);
        }
    }

    private void prepareBoughtItem(final WishlistBundle bundle, final ViewHolder holder) {
        holder.wishlistViewListItemLayout.setBackgroundColor(
                ContextCompat.getColor(getContext(), R.color.material_grey_400));
        // Disable maps and quantity buttons
        disableButton(holder.goToMapActivity);
        disableButton(holder.increaseQuantityButton);
        disableButton(holder.decreaseQuantityButton);

        holder.wishlistViewListItemDeleteBtn.setVisibility(View.VISIBLE);
        holder.wishlistViewListItemDeleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean wasSuccessful = MobilizrApplication.getDbHelper().deleteWishlistEntry(bundle.getWishlistModel());
                if (wasSuccessful) {
                    showToast(Toast.LENGTH_LONG, R.layout.success_toast, R.string.wishlist_item_deleted_confirmation_msg);
                    remove(bundle);
                    notifyDataSetChanged();
                }
                else {
                    UiUtils.snackBarNotify(holder.wishlistViewListItemLayout, mContext.getString(R.string.something_went_wrong_try_again));
                }
            }
        });
    }

    private void disableButton(Button btn) {
        btn.setEnabled(false);
        btn.setBackgroundTintList(new ColorStateList(
                new int[][]{new int[]{-android.R.attr.state_enabled}},
                new int[]{ContextCompat.getColor(getContext(), android.R.color.darker_gray)}
        ));
    }

    /**
     * Displays a toast message defined in the custom toastLayoutId layout.
     * @param toastDuration Obtained from Toast.LENGH_{SHORT, LONG}
     * @param toastLayoutId The id of the custom toast layout (R.layout.X)
     */
    private void showToast(int toastDuration, int toastLayoutId, int strMessageID) {
        Toast t = Toast.makeText(getContext(), "", toastDuration);
        // Load custom toast view
        View view = LayoutInflater.from(getContext()).inflate(
                getContext().getResources().getLayout(toastLayoutId), null);
        ((TextView)view).setText(strMessageID);
        t.setView(view);
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        double toastHeight = displayMetrics.heightPixels * 0.05;
        t.setGravity(Gravity.BOTTOM | Gravity.CENTER_VERTICAL, 0, (int) toastHeight);
        t.show();
        t.show(); // Purposely so that the toast lasts longer
    }
}
