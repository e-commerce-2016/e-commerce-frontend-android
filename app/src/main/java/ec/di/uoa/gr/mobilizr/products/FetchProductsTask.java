package ec.di.uoa.gr.mobilizr.products;

import android.content.Context;
import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;

import ec.di.uoa.gr.mobilizr.R;
import ec.di.uoa.gr.mobilizr.products.product.entities.Product;
import ec.di.uoa.gr.mobilizr.util.ArrayListAsyncTask;
import ec.di.uoa.gr.mobilizr.util.JSONUnMarshaller;
import ec.di.uoa.gr.mobilizr.util.OnArrayListAsyncTaskCompletedListener;

public class FetchProductsTask extends ArrayListAsyncTask<Uri, Void, Product> {
    public FetchProductsTask(final Context context,
                             OnArrayListAsyncTaskCompletedListener<Product> listener) {
        super(context,
                listener,
                new JSONUnMarshaller<Product>() {
            @Override
            public Product unMarshallJSON(JSONObject json) throws JSONException {
                Product product = new Product();
                product.setId(json.getLong(context.getString(R.string.json_id)));
                product.setName(json.getString(context.getString(R.string.product_name)));
                product.setDescription(json.getString(context.getString(R.string.product_description)));
                product.setBase64EncodedImageStr(json.getString(context.getString(R.string.product_image)));
                return product;
            }
        });
    }
}
