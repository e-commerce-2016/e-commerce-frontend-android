package ec.di.uoa.gr.mobilizr;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.StreetViewPanorama;
import com.google.android.gms.maps.StreetViewPanoramaOptions;
import com.google.android.gms.maps.StreetViewPanoramaView;
import com.google.android.gms.maps.model.LatLng;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ec.di.uoa.gr.mobilizr.wishlist.WishlistActivity;
import timber.log.Timber;

public class StreetView extends AppCompatActivity {

    @BindView(R.id.street_view_no_btn)
    Button streetViewNoBtn;
    @BindView(R.id.street_view_yes_btn)
    Button streetViewYesBtn;
    private GoogleApiClient client;
    StreetViewPanorama mStreetViewPanorama;

    private long mWishlistItemId;
    private LatLng mShopLatLng;

    public StreetView() {
        // StreetView empty public constructor

    }

    private StreetViewPanoramaView mStreetViewPanoramaView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_streetview);
        ButterKnife.bind(this);

        MobilizrApplication.getsActionBar().setTitle(R.string.street_view_title);
        MobilizrApplication.getsActionBar().setDisplayHomeAsUpEnabled(true);

        Timber.v("OnStreetView onCreate");
        Intent intent = getIntent();
        mWishlistItemId = intent.getLongExtra(getString(R.string.wishlist_item_id_for_review_key), -1);

        //Bundle bundle = getIntent().getParcelableExtra("bundle");
        //LatLng mShopLatLng = bundle.getParcelable("to");
        mShopLatLng = intent.getParcelableExtra(getString(R.string.street_view_shop_location_key));

        StreetViewPanoramaOptions options = new StreetViewPanoramaOptions();
        if (savedInstanceState == null) {
            options.position(mShopLatLng);
        }
        //new StreetViewDialog();
        mStreetViewPanoramaView = new StreetViewPanoramaView(this, options);

        // Assign StreetViewPanoramaView into frame layout placeholder
        FrameLayout frameLayout = (FrameLayout) findViewById(R.id.street_view_actual_street_view);
        frameLayout.removeAllViews();
        frameLayout.addView(mStreetViewPanoramaView);

        mStreetViewPanoramaView.onCreate(savedInstanceState);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    protected void onResume() {
        mStreetViewPanoramaView.onResume();
        super.onResume();
        Timber.v("OnStreetView onResume");

    }

    @Override
    protected void onPause() {
        mStreetViewPanoramaView.onPause();
        super.onPause();
        Timber.v("OnStreetView onPause");
    }

    @Override
    protected void onDestroy() {
        mStreetViewPanoramaView.onDestroy();
        super.onDestroy();
        Timber.v("OnStreetView onDestroy");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mStreetViewPanoramaView.onSaveInstanceState(outState);
        Timber.v("OnStreetView onSaveInstance");
    }

    @Override
    public void onStart() {
        super.onStart();
        Timber.v("OnStreetViewStart");
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "StreetView Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://ec.di.uoa.gr.mobilizr/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();
        Timber.v("OnStreetViewStop");
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "StreetView Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://ec.di.uoa.gr.mobilizr/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    @OnClick({R.id.street_view_no_btn, R.id.street_view_yes_btn})
    public void onClick(View view) {
        Intent intent = null;
        switch (view.getId()) {
            case R.id.street_view_no_btn:
                intent = new Intent(this, WishlistActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
                break;
            case R.id.street_view_yes_btn:
                intent = new Intent(this, WishlistActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(getString(R.string.wishlist_item_id_for_review_key), mWishlistItemId);
                startActivity(intent);
                finish();
                break;
        }
    }
}
