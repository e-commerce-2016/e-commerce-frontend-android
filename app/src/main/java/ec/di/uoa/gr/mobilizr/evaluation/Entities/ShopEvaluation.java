package ec.di.uoa.gr.mobilizr.evaluation.Entities;


public class ShopEvaluation {
    private String comment;
    private Long rating;
    private Long shop_id;
    private Double price;
    private Long productId;
    private Long ratingCount;
    private Long ratingSum;

    public Long getShopId() {
        return shop_id;
    }

    public void setShopId(Long shopId) {
        this.shop_id = shopId;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Long getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(Long ratingCount) {
        this.ratingCount = ratingCount;
    }

    public Long getRatingSum() {
        return ratingSum;
    }

    public void setRatingSum(Long ratingSum) {
        this.ratingSum = ratingSum;
    }

    @Override
    public String toString() {
        return "Comment[\"" + comment + "\", shop_id: " + shop_id + ", mRating: " + rating + "/5 " +
                "at: "  + "]";
    }
}
