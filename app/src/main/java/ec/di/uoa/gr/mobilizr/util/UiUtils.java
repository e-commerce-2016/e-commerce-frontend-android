package ec.di.uoa.gr.mobilizr.util;

import android.support.design.widget.Snackbar;
import android.view.View;

import ec.di.uoa.gr.mobilizr.MobilizrApplication;
import ec.di.uoa.gr.mobilizr.R;

public class UiUtils {
    public static void snackBarNotify(View rootView, String message) {
        final Snackbar snackBar = Snackbar.make(rootView, message, Snackbar.LENGTH_INDEFINITE);
        snackBar.setAction(MobilizrApplication.getContext().getString(R.string.dismiss_action),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackBar.dismiss();
                    }
                });
        snackBar.show();
    }
}

