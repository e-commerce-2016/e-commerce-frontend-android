package ec.di.uoa.gr.mobilizr.map;

import android.text.Spanned;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

public class Route {
    public Distance distance;
    public Duration duration;
    public String endAddress;
    public LatLng endLocation;
    public String startAddress;
    public LatLng startLocation;
    public List<Spanned> instructions;
    public List<LatLng> points;

    public Route() {
        points = new ArrayList<>();
        instructions = new ArrayList<>();
    }
}
