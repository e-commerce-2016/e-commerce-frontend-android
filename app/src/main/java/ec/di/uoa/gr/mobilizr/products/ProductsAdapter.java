package ec.di.uoa.gr.mobilizr.products;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ec.di.uoa.gr.mobilizr.R;
import ec.di.uoa.gr.mobilizr.products.product.entities.Product;

public class ProductsAdapter extends ArrayAdapter<Product> {
    ViewHolder mViewHolder;
    OnProductSelectedListener mOnProductSelectedListener;

    public ProductsAdapter(Fragment clientFragment, int resource, List<Product> objects) {
        super(clientFragment.getContext(), resource, objects);
        // Ensure that client fragment implements the OnProductShopsBundleReadyListener interface.
        try {
            mOnProductSelectedListener = (OnProductSelectedListener) clientFragment;
        }
        catch (ClassCastException e) {
            throw new RuntimeException(clientFragment.getClass().toString() + " must implement " +
                    OnProductSelectedListener.class.toString() + " interface");
        }

    }

    static class ViewHolder {
        TextView productNameTextView;
        TextView productDescriptionTextView;
        ImageView productImageView;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.product_list_item, parent, false);

            // Lookup views and set view holder.
            mViewHolder = new ViewHolder();
            mViewHolder.productNameTextView = (TextView) convertView.findViewById(R.id.products_view_product_name);
            mViewHolder.productDescriptionTextView = (TextView) convertView.findViewById(R.id.products_view_product_description);
            mViewHolder.productImageView = (ImageView) convertView.findViewById(R.id.products_view_product_image);

            // Initialize view's content.
            Product product = getItem(position);
            mViewHolder.productNameTextView.setText(product.getName());
            mViewHolder.productDescriptionTextView.setText(product.getDescription());
            if (product.getImageBitmap() != null)
                mViewHolder.productImageView.setImageBitmap(product.getImageBitmap().getBitmap());
            else
                // Couldn't retrieve image bitmap, display the ic_error_black icon.
                mViewHolder.productImageView.setImageResource(R.drawable.ic_error_black_24dp);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnProductSelectedListener.onProductSelected(getItem(position));
            }
        });

        return convertView;
    }

    /**
     * Callback for notifying the fragment for the product selected event.
     */
    public interface OnProductSelectedListener {
        public void onProductSelected(Product selectedProduct);
    }
}
