package ec.di.uoa.gr.mobilizr.shops.soap;




//----------------------------------------------------
//
// Generated by www.easywsdl.com
// Version: 4.5.8.1
//
// Created by Quasar Development at 12/09/2016
//
//---------------------------------------------------




import org.ksoap2.HeaderProperty;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.transport.HttpTransportSE;
import org.ksoap2.transport.HttpsTransportSE;
import org.ksoap2.transport.Transport;

import java.util.List;


public class FJDFeedbackHandlerImplementationPortBinding
{
    interface FJDIWcfMethod
    {
        FJDExtendedSoapSerializationEnvelope CreateSoapEnvelope() throws Exception;

        Object ProcessResult(FJDExtendedSoapSerializationEnvelope __envelope, Object result) throws Exception;
    }

    String url;

    int timeOut=60000;
    public List< HeaderProperty> httpHeaders;
    public boolean enableLogging;

    FJDIServiceEvents callback;
    public FJDFeedbackHandlerImplementationPortBinding(){}
    public FJDFeedbackHandlerImplementationPortBinding(String url){this.url = url;}

    public FJDFeedbackHandlerImplementationPortBinding (FJDIServiceEvents callback)
    {
        this.callback = callback;
    }
    public FJDFeedbackHandlerImplementationPortBinding(FJDIServiceEvents callback,String url)
    {
        this.callback = callback;
        this.url = url;
    }

    public FJDFeedbackHandlerImplementationPortBinding(FJDIServiceEvents callback,String url,int timeOut)
    {
        this.callback = callback;
        this.url = url;
        this.timeOut=timeOut;
    }

    protected Transport createTransport()
    {
        try
        {
            java.net.URI uri = new java.net.URI(url);
            if(uri.getScheme().equalsIgnoreCase("https"))
            {
                int port=uri.getPort()>0?uri.getPort():443;
                return new HttpsTransportSE(uri.getHost(), port, uri.getPath(), timeOut);
            }
            else
            {
                return new HttpTransportSE(url,timeOut);
            }

        }
        catch (java.net.URISyntaxException e)
        {
        }
        return null;
    }
    
    protected FJDExtendedSoapSerializationEnvelope createEnvelope()
    {
        FJDExtendedSoapSerializationEnvelope envelope= new FJDExtendedSoapSerializationEnvelope(FJDExtendedSoapSerializationEnvelope.VER11);
        return envelope;
    }
    
    protected List sendRequest(String methodName,FJDExtendedSoapSerializationEnvelope envelope,Transport transport  )throws Exception
    {
        return transport.call(methodName, envelope, httpHeaders);
    }

    Object getResult(Class destObj,Object source,String resultName,FJDExtendedSoapSerializationEnvelope __envelope) throws Exception
    {
        if(source==null)
        {
            return null;
        }
        if(source instanceof SoapPrimitive)
        {
            SoapPrimitive soap =(SoapPrimitive)source;
            if(soap.getName().equals(resultName))
            {
                Object instance=__envelope.get(source,destObj);
                return instance;
            }
        }
        else
        {
            SoapObject soap = (SoapObject)source;
            if (soap.hasProperty(resultName))
            {
                Object j=soap.getProperty(resultName);
                if(j==null)
                {
                    return null;
                }
                Object instance=__envelope.get(j,destObj);
                return instance;
            }
            else if( soap.getName().equals(resultName)) {
                Object instance=__envelope.get(source,destObj);
                return instance;
            }
       }

       return null;
    }

        
    public Boolean postFeedback(final Long shop,final Long user,final String comment,final Long rating,final Double realPrice ) throws Exception
    {
        return (Boolean)execute(new FJDIWcfMethod()
        {
            @Override
            public FJDExtendedSoapSerializationEnvelope CreateSoapEnvelope(){
              FJDExtendedSoapSerializationEnvelope __envelope = createEnvelope();
                SoapObject __soapReq = new SoapObject("http://soap.webservices.feedback.mobilizr.com/", "postFeedback");
                __envelope.setOutputSoapObject(__soapReq);
                
                PropertyInfo __info=null;
                __info = new PropertyInfo();
                __info.namespace="";
                __info.name="shop";
                __info.type=PropertyInfo.LONG_CLASS;
                __info.setValue(shop!=null?shop:SoapPrimitive.NullNilElement);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="";
                __info.name="user";
                __info.type=PropertyInfo.LONG_CLASS;
                __info.setValue(user!=null?user:SoapPrimitive.NullNilElement);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="";
                __info.name="comment";
                __info.type=PropertyInfo.STRING_CLASS;
                __info.setValue(comment!=null?comment:SoapPrimitive.NullNilElement);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="";
                __info.name="rating";
                __info.type=PropertyInfo.LONG_CLASS;
                __info.setValue(rating!=null?rating:SoapPrimitive.NullNilElement);
                __soapReq.addProperty(__info);
                __info = new PropertyInfo();
                __info.namespace="";
                __info.name="realPrice";
                __info.type=Double.class;
                __info.setValue(realPrice!=null?realPrice:SoapPrimitive.NullNilElement);
                __soapReq.addProperty(__info);
                return __envelope;
            }
            
            @Override
            public Object ProcessResult(FJDExtendedSoapSerializationEnvelope __envelope,Object __result)throws Exception {
                SoapObject __soap=(SoapObject)__result;
                Object obj = __soap.getProperty("return");
                if (obj != null && obj.getClass().equals(SoapPrimitive.class))
                {
                    SoapPrimitive j =(SoapPrimitive) obj;
                    return new Boolean(j.toString());
                }
                else if (obj!= null && obj instanceof Boolean){
                    return (Boolean)obj;
                }
                return null;
            }
        },"insertFeedbackAction");
    }
    
    public android.os.AsyncTask< Void, Void, FJDOperationResult< Boolean>> postFeedbackAsync(final Long shop,final Long user,final String comment,final Long rating,final Double realPrice)
    {
        return executeAsync(new FJDFunctions.IFunc< Boolean>() {
            public Boolean Func() throws Exception {
                return postFeedback( shop,user,comment,rating,realPrice);
            }
        });
    }

    
    protected Object execute(FJDIWcfMethod wcfMethod,String methodName) throws Exception
    {
        Transport __httpTransport=createTransport();
        __httpTransport.debug=enableLogging;
        FJDExtendedSoapSerializationEnvelope __envelope=wcfMethod.CreateSoapEnvelope();
        try
        {
            sendRequest(methodName, __envelope, __httpTransport);
            
        }
        finally {
            if (__httpTransport.debug) {
                if (__httpTransport.requestDump != null) {
                    android.util.Log.i("requestDump",__httpTransport.requestDump);    
                    
                }
                if (__httpTransport.responseDump != null) {
                    android.util.Log.i("responseDump",__httpTransport.responseDump);
                }
            }
        }
        Object __retObj = __envelope.bodyIn;
        if (__retObj instanceof org.ksoap2.SoapFault){
            org.ksoap2.SoapFault __fault = (org.ksoap2.SoapFault)__retObj;
            throw convertToException(__fault,__envelope);
        }else{
            return wcfMethod.ProcessResult(__envelope,__retObj);
        }
    }
    
    protected < T> android.os.AsyncTask< Void, Void, FJDOperationResult< T>>  executeAsync(final FJDFunctions.IFunc< T> func)
    {
        return new android.os.AsyncTask< Void, Void, FJDOperationResult< T>>()
        {
            @Override
            protected void onPreExecute() {
                callback.Starting();
            };
            @Override
            protected FJDOperationResult< T> doInBackground(Void... params) {
                FJDOperationResult< T> result = new FJDOperationResult< T>();
                try
                {
                    result.Result= func.Func();
                }
                catch(Exception ex)
                {
                    ex.printStackTrace();
                    result.Exception=ex;
                }
                return result;
            }
            
            @Override
            protected void onPostExecute(FJDOperationResult< T> result)
            {
                callback.Completed(result);
            }
        }.execute();
    }
        
    Exception convertToException(org.ksoap2.SoapFault fault,FJDExtendedSoapSerializationEnvelope envelope)
    {

        return new Exception(fault.faultstring);
    }
}


