package ec.di.uoa.gr.mobilizr.util;

import org.json.JSONException;
import org.json.JSONObject;

public interface JSONUnMarshaller<T> {
    T unMarshallJSON(JSONObject json) throws JSONException;
}
