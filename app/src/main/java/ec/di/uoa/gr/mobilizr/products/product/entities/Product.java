package ec.di.uoa.gr.mobilizr.products.product.entities;

import java.io.Serializable;

import ec.di.uoa.gr.mobilizr.util.media.SerialBitmap;

public class Product implements Serializable {
    private static final long serialVersionUID = -5712360492133626256L;

    private Long id;
    private String name;
    private String description;
    private String base64EncodedImageStr;
    private SerialBitmap imageBitmap = null;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBase64EncodedImageStr() {
        return base64EncodedImageStr;
    }

    public void setBase64EncodedImageStr(String base64EncodedImageStr) {
        this.base64EncodedImageStr = base64EncodedImageStr;
    }

    public SerialBitmap getImageBitmap() {
        return imageBitmap;
    }

    public void setImageBitmap(SerialBitmap imageBitmap) {
        this.imageBitmap = imageBitmap;
    }

    @Override
    public String toString() {
        return "Product[id=" + id + ", name=\"" + name + "\", description=\"" + description + "\"";
    }
}