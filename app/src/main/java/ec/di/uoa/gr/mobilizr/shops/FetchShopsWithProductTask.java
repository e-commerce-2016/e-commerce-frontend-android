package ec.di.uoa.gr.mobilizr.shops;

import android.content.Context;
import android.net.Uri;

import org.json.JSONException;
import org.json.JSONObject;

import ec.di.uoa.gr.mobilizr.R;
import ec.di.uoa.gr.mobilizr.shops.entities.Shop;
import ec.di.uoa.gr.mobilizr.shops.entities.ShopHasProduct;
import ec.di.uoa.gr.mobilizr.shops.entities.ShopHasProductAggregated;
import ec.di.uoa.gr.mobilizr.util.ArrayListAsyncTask;
import ec.di.uoa.gr.mobilizr.util.JSONUnMarshaller;
import ec.di.uoa.gr.mobilizr.util.OnArrayListAsyncTaskCompletedListener;


public class FetchShopsWithProductTask extends ArrayListAsyncTask<Uri, Void, ShopHasProductAggregated> {
    /**
     * @param context
     * @param listener  Interface instance whose method will be called at OnPostExecute to handle
     *                  the populated ArrayList<ContainedType>.
     */
    public FetchShopsWithProductTask(
            final Context context,
            OnArrayListAsyncTaskCompletedListener<ShopHasProductAggregated> listener) {
        super(context, listener, new JSONUnMarshaller<ShopHasProductAggregated>() {
            @Override
            public ShopHasProductAggregated unMarshallJSON(JSONObject json) throws JSONException {
                ShopHasProductAggregated shopHasProductAggregated = new ShopHasProductAggregated();
                Shop shop = shopHasProductAggregated.getShop();
                ShopHasProduct sHP = shopHasProductAggregated.getShopHasProduct();

                sHP.setId(json.getLong(context.getString(R.string.json_id)));
                sHP.setProductId(json.getLong(context.getString(R.string.product_id)));
                sHP.setPrice(json.getDouble(context.getString(R.string.json_shop_has_product_price)));
                sHP.setQuantity(json.getLong(context.getString(R.string.json_shop_has_product_quantity)));

                JSONObject shopJson = json.getJSONObject(context.getString(R.string.json_shop_key));
                Long shopId = shopJson.getLong(context.getString(R.string.json_id));
                sHP.setShopId(shopId);
                shop.setId(shopId);
                shop.setAddress(shopJson.getString(context.getString(R.string.json_shop_address)));
                shop.setBrand(shopJson.getString(context.getString(R.string.json_shop_brand)));
                shop.setCity(shopJson.getString(context.getString(R.string.json_shop_city)));
                shop.setLat(shopJson.getDouble(context.getString(R.string.json_lat)));
                shop.setLon(shopJson.getDouble(context.getString(R.string.json_lon)));
                shop.setPhone(shopJson.getString(context.getString(R.string.json_shop_phone)));
                shop.setPostalCode(shopJson.getString(context.getString(R.string.json_shop_postal_code)));
                shop.setRatingCount(shopJson.getLong(context.getString(R.string.json_shop_rating_count)));
                shop.setRatingSum(shopJson.getLong(context.getString(R.string.json_shop_rating_sum)));
                shop.setTrn(shopJson.getString(context.getString(R.string.json_shop_trn)));

                return shopHasProductAggregated;
            }
        });
    }
}
