package ec.di.uoa.gr.mobilizr.products.product.entities;

import android.view.View;

import ec.di.uoa.gr.mobilizr.products.product.SelectProductAsyncTask;
import ec.di.uoa.gr.mobilizr.shops.entities.ShopHasProductAggregated;

public class ProductViewAsyncTaskBundle {
    ShopHasProductAggregated shopHasProductAggregated;
    SelectProductAsyncTask.OnOutcomeReadyListener.StatusCode statusCode;
    // The adapter view whose button was clicked and initiated the task, needed for UI feedback.
    View adapterViewItem;

    public ProductViewAsyncTaskBundle(ShopHasProductAggregated shopHasProductAggregated,
                                      SelectProductAsyncTask.OnOutcomeReadyListener.StatusCode statusCode,
                                      View adapterViewItem) {
        this.shopHasProductAggregated = shopHasProductAggregated;
        this.statusCode = statusCode;
        this.adapterViewItem = adapterViewItem;
    }

    public ShopHasProductAggregated getShopHasProductAggregated() {
        return shopHasProductAggregated;
    }

    public void setShopHasProductAggregated(ShopHasProductAggregated shopHasProductAggregated) {
        this.shopHasProductAggregated = shopHasProductAggregated;
    }

    public SelectProductAsyncTask.OnOutcomeReadyListener.StatusCode getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(SelectProductAsyncTask.OnOutcomeReadyListener.StatusCode statusCode) {
        this.statusCode = statusCode;
    }

    public View getAdapterViewItem() {
        return adapterViewItem;
    }

    public void setAdapterViewItem(View adapterViewItem) {
        this.adapterViewItem = adapterViewItem;
    }
}