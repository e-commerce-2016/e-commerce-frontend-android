package ec.di.uoa.gr.mobilizr.data;

public class MobilizrContract {
    // Prevent instantiation
    private MobilizrContract() {}

    public static String DATABASE_NAME = "mobilizr.db";

    public static final class ShopTable {
        public static final String TABLE_NAME = "shop";

        public static final String COLUMN_ID = "id";
        public static final String COLUMN_BRAND = "brand";
        public static final String COLUMN_LAT = "lat";
        public static final String COLUMN_LON = "lon";
        public static final String COLUMN_ADDRESS = "address";
        public static final String COLUMN_CITY = "city";
        public static final String COLUMN_POSTAL_CODE = "postal_code";
        public static final String COLUMN_PHONE = "phone";
        public static final String COLUMN_MOBILE_PHONE = "mobile_phone";
        public static final String COLUMN_TRN = "trn";
        public static final String COLUMN_RATING_SUM = "rating_sum";
        public static final String COLUMN_RATING_COUNT = "rating_count";

        public static final String SQL_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME +
                "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY," +
                COLUMN_BRAND + " TEXT NOT NULL," +
                COLUMN_LAT + " REAL NOT NULL," +
                COLUMN_LON + " REAL NOT NULL," +
                COLUMN_ADDRESS + " TEXT NOT NULL," +
                COLUMN_CITY + " TEXT NOT NULL," +
                COLUMN_POSTAL_CODE + " TEXT NOT NULL," +
                COLUMN_PHONE + " TEXT NOT NULL," +
                COLUMN_MOBILE_PHONE + " TEXT," +
                COLUMN_TRN + " TEXT," +
                COLUMN_RATING_SUM + " REAL NOT NULL," +
                COLUMN_RATING_COUNT + " INTEGER NOT NULL" +
                ")";
    }

    public static final class ProductTable {
        public static final String TABLE_NAME = "product";

        public static final String COLUMN_ID = "id";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_ENCODED_IMAGE = "base64EncodedImageStr";

        public static final String SQL_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME +
                "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY," +
                COLUMN_NAME + " TEXT NOT NULL," +
                COLUMN_DESCRIPTION + " TEXT NOT NULL," +
                COLUMN_ENCODED_IMAGE + " TEXT NOT NULL" +
                ")";
    }


    public static final class WishlistTable {
        public static final String TABLE_NAME = "wishlist";

        public static final String COLUMN_ID = "id";
        public static final String COLUMN_SHOP_ID_FK = "shop_id";
        public static final String COLUMN_PRODUCT_ID_FK = "product_id";
        public static final String COLUMN_PRICE = "price";
        public static final String COLUMN_QUANTITY = "quantity";
        public static final String COLUMN_WAS_BOUGHT = "was_bought";

        public static final String SQL_CREATE_TABLE = "CREATE TABLE " + TABLE_NAME +
                "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY," +
                COLUMN_SHOP_ID_FK + " INTEGER REFERENCES " + ShopTable.TABLE_NAME + "," +
                COLUMN_PRODUCT_ID_FK + " INTEGER REFERENCES " + ProductTable.TABLE_NAME + "," +
                COLUMN_PRICE + " REAL NOT NULL," +
                COLUMN_QUANTITY + " INTEGER NOT NULL," +
                COLUMN_WAS_BOUGHT + " INTEGER NOT NULL" +
                ")";
    }

}
