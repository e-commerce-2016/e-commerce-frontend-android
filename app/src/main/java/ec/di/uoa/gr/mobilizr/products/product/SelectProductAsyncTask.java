package ec.di.uoa.gr.mobilizr.products.product;

import android.os.AsyncTask;
import android.support.v4.app.Fragment;

import ec.di.uoa.gr.mobilizr.MobilizrApplication;
import ec.di.uoa.gr.mobilizr.data.MobilizrDbHelper;
import ec.di.uoa.gr.mobilizr.products.product.entities.Product;
import ec.di.uoa.gr.mobilizr.products.product.entities.ProductViewAsyncTaskBundle;
import ec.di.uoa.gr.mobilizr.products.product.soap.OROProductQuantityHandlerImplementationPortBinding;
import ec.di.uoa.gr.mobilizr.util.BackendUtils;
import timber.log.Timber;

public class SelectProductAsyncTask extends AsyncTask<Void, Void,
        SelectProductAsyncTask.OnOutcomeReadyListener.StatusCode>
{
    private static final String sNAMESPACE = "http://soap.webservices.shop_has_product.mobilizr.com/";
    private static String sURL = "http://" +
            BackendUtils.getBackendIP(MobilizrApplication.getContext()) +
            ":8080/mobilizr/ws/ProductQuantityHandlerImplementation?wsdl";
    private static final String sMETHOD_NAME = "reduceQuantity";
    private static final String sSOAP_ACTION = "http://soap.webservices.shop_has_product.mobilizr.com/ProductQuantityHandler/reduceQuantity";

    private final Product mProduct;
    private final MobilizrDbHelper mDbHelper;
    private final OnOutcomeReadyListener mOnOutcomeReadyListener;
    private final ProductViewAsyncTaskBundle mProductViewAsyncTaskBundle;

    public SelectProductAsyncTask(Fragment clientFragment,
                                  ProductViewAsyncTaskBundle bundle,
                                  Product product,
                                  MobilizrDbHelper mDbHelper) {
        this.mProductViewAsyncTaskBundle = bundle;
        this.mProduct = product;
        this.mDbHelper = mDbHelper;
        try {
            mOnOutcomeReadyListener = (OnOutcomeReadyListener)
                    ((ProductView) clientFragment).getmShopsHaveProductAggregatedAdapter();
        }
        catch (ClassCastException e) {
            // Must fail.
            throw new ClassCastException(
                    ((ProductView) clientFragment).getmShopsHaveProductAggregatedAdapter()
                            .getClass().getCanonicalName() + " must implement interface: "
                            + OnOutcomeReadyListener.class.getCanonicalName());
        }
    }

    @Override
    protected OnOutcomeReadyListener.StatusCode doInBackground(Void... params) {
        OROProductQuantityHandlerImplementationPortBinding service =
                new OROProductQuantityHandlerImplementationPortBinding(sURL);
        Timber.i("Reducing quantity of " + mProduct.getName() + " from shop: " +
                mProductViewAsyncTaskBundle.getShopHasProductAggregated().getShop().getBrand());

        Boolean isAvailable;
        try {
            // Try to reduce mProduct quantity, while checking availability
            isAvailable = service.reduceQuantity(
                    mProductViewAsyncTaskBundle.getShopHasProductAggregated().getShop().getId(),
                    mProduct.getId());
        } catch (Exception e) {
            Timber.e(e, "ReduceQuantity failed.");
            return OnOutcomeReadyListener.StatusCode.FAILURE;
        }

        if (isAvailable) {
            boolean wasSuccessful = mDbHelper.addOrUpdateWishlist(null, mProduct,
                    mProductViewAsyncTaskBundle.getShopHasProductAggregated(),
                    MobilizrDbHelper.QUANTITY_HANDLING_MODE.ADD_OR_INCREMENT);
            if (!wasSuccessful)
                return OnOutcomeReadyListener.StatusCode.FAILURE;
        }

        return OnOutcomeReadyListener.StatusCode.SUCCESS;
    }

    @Override
    protected void onPostExecute(OnOutcomeReadyListener.StatusCode result) {
        mProductViewAsyncTaskBundle.setStatusCode(result);
        mOnOutcomeReadyListener.onOutcomeReady(mProductViewAsyncTaskBundle);
    }

    /**
     * Interface for obtaining and returning the outcome of the SelectProductAsyncTask.
     */
    public interface OnOutcomeReadyListener {
        enum StatusCode {
            NO_AVAILABILITY,
            FAILURE,
            SUCCESS
        }

        /**
         * @param statusCode    The status code returned from SelectProductAsyncTask.
         */
        void onOutcomeReady(ProductViewAsyncTaskBundle statusCode);
    }
}
