package ec.di.uoa.gr.mobilizr.util.persistence;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import ec.di.uoa.gr.mobilizr.products.product.entities.Product;
import ec.di.uoa.gr.mobilizr.shops.entities.ProductShopsBundle;
import timber.log.Timber;

public class ResultsCache {
    private static ResultsCache instance = null;
    private static final String persistedProductsFilename = "mobilizr-cache-products";
    private static final String persistedProductShopsBundleFilename = "mobilizr-cache-bundle";
    // Instance members ----------------------------------------------------------------------------
    private ArrayList<Product> mProducts = null;
    private ProductShopsBundle mProductShopsBundle = null;
    private Context mCtx;

    // Singleton -----------------------------------------------------------------------------------
    private ResultsCache(Context ctx) {
        mCtx = ctx;
        Timber.d("ResultsCache instantiated. AppDir is: %s", mCtx.getFilesDir());
    }

    public static ResultsCache getInstance(Context ctx) {
        if (instance == null) {
            instance = new ResultsCache(ctx);
        }
        return instance;
    }

    // Public API ----------------------------------------------------------------------------------
    /**
     * Serializes data into app's internal directory.
     * <b>Use deSerializeData() to read back from storage.</b>
     */
    public void serializeData() {
        if (isProductsListSet())
            serializeObject(persistedProductsFilename, mProducts);

        if (isProductShopsBundleSet())
            serializeObject(persistedProductShopsBundleFilename, mProductShopsBundle);
    }

    /**
     * De-serializes data from app's internal directory.
     */
    public void deSerializeData() {
        if (mProducts == null)
            mProducts = deSerializeObject(persistedProductsFilename);

        if (mProductShopsBundle == null)
            mProductShopsBundle = deSerializeObject(persistedProductShopsBundleFilename);
    }

    /**
     * Used for checking if products list is set before any action.
     * @return True if set, false otherwise.
     */
    public boolean isProductsListSet() {
        return mProducts != null;
    }

    /**
     * Used for checking if ProductShopsBundle list is set before any action.
     * @return True if set, false otherwise.
     */
    public boolean isProductShopsBundleSet() {
        return mProductShopsBundle != null;
    }

    public void clearCache() {
        Timber.v("Clearing cache...");
        clearInternalStorageFile(persistedProductsFilename);
        clearInternalStorageFile(persistedProductShopsBundleFilename);
        mProductShopsBundle = null;
        mProducts = null;
    }

    // Helpers -------------------------------------------------------------------------------------
    /**
     * Serializes the passed object to a file in internal app storage.
     * @param filename The filename that the object is to be serialized to.
     * @param toBeSerialized The object that will be serialized.
     * @throws IOException
     */
    private void serializeObject(String filename, Object toBeSerialized) {
        // Followed this approach with exceptions so that we can be able to pinpoint the exact
        // statement that fails. (Chained calls hide it and application crashes without any logs.)
        File f = new File(mCtx.getFilesDir() + "/" + filename);
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = mCtx.openFileOutput(f.getName(), mCtx.MODE_PRIVATE);
        } catch (FileNotFoundException e) {
            Timber.e("IOException when opening serialized: %s.", e.getMessage());
            return;
        }
        ObjectOutputStream objectOutputStream = null;
        try {
            objectOutputStream = new ObjectOutputStream(fileOutputStream);
        } catch (IOException e) {
            Timber.e("IOException when creating ObjectOutputStream: %s.", e.getMessage());
            return;
        }
        try {
            objectOutputStream.writeObject(toBeSerialized);
        } catch (IOException e) {
            Timber.e("IOException when writing object: %s.", e.getMessage());
            return;
        }
        try {
            objectOutputStream.close();
        } catch (IOException e) {
            Timber.e("IOException when closing ObjectOutputStream: %s.", e.getMessage());
            return;
        }
        try {
            fileOutputStream.close();
        } catch (IOException e) {
            Timber.e("IOException when closing FileOutputStream: %s.", e.getMessage());
            return;
        }
        Timber.d("Serialized object to file [%s].", f);
    }

    /**
     * De-serialized the object from the specified file from internal app storage.
     * @param filename The filename of the object to be de-serialized.
     * @return The object that was de-serialized.
     */
    private <T> T deSerializeObject(String filename) {
        // Followed this approach with exceptions so that we can be able to pinpoint the exact
        // statement that fails. (Chained calls hide it and application crashes without any logs.)
        File f = new File(mCtx.getFilesDir() + "/" + filename);
        if (!f.exists()) {
            Timber.v("Nothing to de-serialize for file[%s].", filename);
            return null;
        }
        FileInputStream fileInputStream = null;
        try {
            fileInputStream = mCtx.openFileInput(f.getName());
        } catch (FileNotFoundException e) {
            Timber.e("File[%s] not found: %s", f.getName(), e.getMessage());
            return null;
        }
        ObjectInputStream objectInputStream = null;
        try {
            objectInputStream = new ObjectInputStream(fileInputStream);
        } catch (IOException e) {
            Timber.e("IOException when opening ObjectInputStream: %s", e.getMessage());
            return null;
        }
        T o = null;
        try {
            o = (T) objectInputStream.readObject();
        } catch (ClassNotFoundException e) {
            Timber.e("ClassNotFoundException when casting read object: %s", e.getMessage());
            return null;
        } catch (IOException e) {
            Timber.e("IOException when reading from ObjectInputStream: %s", e.getMessage());
            return null;
        }
        try {
            objectInputStream.close();
        } catch (IOException e) {
            Timber.e("Closing ObjectOutputStream: %s", e.getMessage());
            return null;
        }
        try {
            fileInputStream.close();
        } catch (IOException e) {
            Timber.e("Closing ObjectOutputStream: %s", e.getMessage());
            return null;
        }
        Timber.d("De-serialized file [%s].", f);
        return o;
    }

    /**
     * Deletes the file specified from internal storage directory.
     * @param filename The filename of the file to be deleted.
     */
    private void clearInternalStorageFile(String filename) {
        File f = new File(mCtx.getFilesDir() + "/" + filename);
        f.delete();
    }


    // Getters / Setters ---------------------------------------------------------------------------
    public ArrayList<Product> getProducts() {
        return mProducts;
    }

    public void setProducts(ArrayList<Product> mProducts) {
        this.mProducts = mProducts;
    }

    public ProductShopsBundle getProductShopsBundle() {
        return mProductShopsBundle;
    }

    public void setProductShopsBundle(ProductShopsBundle mProductShopsBundle) {
        this.mProductShopsBundle = mProductShopsBundle;
    }
}
