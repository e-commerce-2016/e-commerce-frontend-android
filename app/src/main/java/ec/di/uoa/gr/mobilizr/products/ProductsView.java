package ec.di.uoa.gr.mobilizr.products;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import ec.di.uoa.gr.mobilizr.MobilizrApplication;
import ec.di.uoa.gr.mobilizr.R;
import ec.di.uoa.gr.mobilizr.products.product.entities.Product;
import ec.di.uoa.gr.mobilizr.shops.FetchShopsWithProductTask;
import ec.di.uoa.gr.mobilizr.shops.entities.ProductShopsBundle;
import ec.di.uoa.gr.mobilizr.shops.entities.ShopHasProductAggregated;
import ec.di.uoa.gr.mobilizr.util.BackendUtils;
import ec.di.uoa.gr.mobilizr.util.OnArrayListAsyncTaskCompletedListener;

public class ProductsView extends Fragment
    implements ProductsAdapter.OnProductSelectedListener {
    private ProductsAdapter mProductAdapter;
    private ArrayList<Product> mProductsList;
    private OnProductShopsBundleReadyListener mOnProductShopsBundleReadyListener;
    public static final String LOG_TAG = ProductsView.class.getSimpleName();
    private View mRootView;

    public ProductsView() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param products_search_results_list_key The key of the products search result list.
     * @param productsList The list containing the products returned from initial search.
     * @return A new instance of fragment ProductsView.
     */
    public static ProductsView newInstance(String products_search_results_list_key,
                                           ArrayList<Product> productsList) {
        ProductsView fragment = new ProductsView();
        Bundle args = new Bundle();
        args.putSerializable(products_search_results_list_key, productsList);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mProductsList = (ArrayList<Product>) getArguments().get(getResources()
                    .getString(R.string.products_search_results_list_key));
        }

        mProductAdapter = new ProductsAdapter(
                this,
                R.layout.product_list_item,
                mProductsList
        );
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Get rootView to access the ListView and set its adapter
        mRootView = inflater.inflate(R.layout.fragment_products_view, container, false);

        MobilizrApplication.getsActionBar().setTitle(R.string.toolbar_title_products);
        MobilizrApplication.getsActionBar().setDisplayHomeAsUpEnabled(true);

        ListView productListView = (ListView) mRootView.findViewById(R.id.products_list_view);
        productListView.setAdapter(mProductAdapter);

        return mRootView;
    }

    @Override
    public void onProductSelected(final Product selectedProduct) {
        // Build Uri
        Integer productsGetEndpointIdx = getResources().getInteger(R.integer.shopsWithProductRS);
        String endpointUrl =
                getResources().getStringArray(R.array.webServicesEntrypoints)[productsGetEndpointIdx];
        String endpoint = BackendUtils.prepareEndpointURL(getContext(), endpointUrl);
        String productId = selectedProduct.getId().toString();
        Uri builtUri = Uri.parse(endpoint).buildUpon()
                .appendPath(productId).build();

        new FetchShopsWithProductTask(getContext(), new OnArrayListAsyncTaskCompletedListener<ShopHasProductAggregated>() {
            @Override
            public void onAsyncTaskCompleted(ArrayList<ShopHasProductAggregated> resultsArrayList) {
                if (resultsArrayList == null) {
                    final Snackbar snackbar =
                            Snackbar.make(mRootView, R.string.connection_error_message,
                                    Snackbar.LENGTH_INDEFINITE);

                    snackbar.setAction(R.string.dismiss_action,
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    snackbar.dismiss();
                                }
                            });
                    snackbar.show();
                    return;
                }
                ProductShopsBundle productShopsBundle =
                        new ProductShopsBundle(selectedProduct, resultsArrayList);
                mOnProductShopsBundleReadyListener.onProductShopsBundleReady(productShopsBundle);
            }
        }).execute(builtUri);
    }

    // Must be implemented by parent activity in order to notify it for the search results
    // and to initiate the ProductView fragment
    public interface OnProductShopsBundleReadyListener {
        void onProductShopsBundleReady(ProductShopsBundle productShopsBundle);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity currentActivity = getActivity();
        try {
             mOnProductShopsBundleReadyListener = (OnProductShopsBundleReadyListener) currentActivity;
        } catch (ClassCastException e) {
            throw new ClassCastException(currentActivity.toString() + " must implement " +
                    OnProductShopsBundleReadyListener.class);
        }
    }
}
