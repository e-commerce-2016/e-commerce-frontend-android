package ec.di.uoa.gr.mobilizr.shops;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import ec.di.uoa.gr.mobilizr.MobilizrApplication;
import ec.di.uoa.gr.mobilizr.R;
import ec.di.uoa.gr.mobilizr.shops.entities.Comment;
import ec.di.uoa.gr.mobilizr.shops.entities.Shop;

public class ShopView extends Fragment {
    private Shop mShop;
    private ArrayList<Comment> mComments;
    private CommentsArrayAdapter mCommentsArrayAdapter;

    public ShopView() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param shopViewShop The key for retrieving the serializable shop object @ onCreate.
     * @param shop         The shop of the view.
     * @return A new instance of fragment ShopView.
     */
    // TODO: Rename and change types and number of parameters
    public static ShopView newInstance(
            String shopViewShop, Shop shop,
            String shopViewComments, ArrayList<Comment> comments) {
        ShopView fragment = new ShopView();
        Bundle args = new Bundle();
        args.putSerializable(shopViewShop, shop);
        args.putSerializable(shopViewComments, comments);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mShop = (Shop) getArguments().getSerializable(getString(R.string.shop_view_shop_argument_key));
            mComments = (ArrayList<Comment>)
                    getArguments().getSerializable(getString(R.string.shop_view_shop_comments_argument_key));
        }

        mCommentsArrayAdapter =
                new CommentsArrayAdapter(this, R.id.shop_view_comments_list_item, mComments);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        RelativeLayout rootView = (RelativeLayout)
                inflater.inflate(R.layout.shop_view_container_layout, container, false);

        MobilizrApplication.getsActionBar().setTitle(R.string.toolbar_title_shop);
        MobilizrApplication.getsActionBar().setDisplayHomeAsUpEnabled(true);

        // Shop brand and mRating views
        RelativeLayout shopBrandRatingLayout = (RelativeLayout)
                rootView.findViewById(R.id.shop_view_brand_rating_layout);

        TextView shopBrandTextView = (TextView)
                shopBrandRatingLayout.findViewById(R.id.shop_view_shop_brand);
        shopBrandTextView.setText(mShop.getBrand());

        //      Rating handling
        TextView shopRatingTextView = (TextView)
                shopBrandRatingLayout.findViewById(R.id.shop_view_rating_text);
        RatingBar shopRatingBar = (RatingBar)
                shopBrandRatingLayout.findViewById(R.id.shop_view_ratingbar);
        if (mShop.getRatingCount() != 0) {
            long shopRating = mShop.getRatingSum() / mShop.getRatingCount();
            shopRatingBar.setRating(shopRating);
        } else {
            shopRatingBar.setVisibility(View.GONE);
            shopRatingTextView.setVisibility(View.VISIBLE);
        }

        // Shop details
        LinearLayout shopDetailsLayout = (LinearLayout)
                rootView.findViewById(R.id.shop_view_shop_details_layout);
        TextView shopLocationTextView = (TextView)
                shopDetailsLayout.findViewById(R.id.shop_location);
        String shopLocationStr = mShop.getAddress() + ", " + mShop.getCity();
        shopLocationTextView.setText(shopLocationStr);

        TextView shopPhonesTextView = (TextView)
                shopDetailsLayout.findViewById(R.id.shop_view_shop_phones);
        String shopPhones = mShop.getPhone();
        if (mShop.getMobilePhone() != null)
            shopPhones += ", " + mShop.getMobilePhone();
        shopPhonesTextView.setText(shopPhones);

        ListView commentsListView = (ListView)
                rootView.findViewById(R.id.shop_view_comments_listview);
        commentsListView.setAdapter(mCommentsArrayAdapter);

        return rootView;
    }
}
