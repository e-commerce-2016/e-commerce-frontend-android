package ec.di.uoa.gr.mobilizr.shops.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ec.di.uoa.gr.mobilizr.products.product.entities.Product;

/**
 * Bundle of needed objects for displaying the ProductView (Product and a list of shops that offer
 * it).
 */
public class ProductShopsBundle implements Serializable {
    private static final long serialVersionUID = 9150396165144732629L;
    Product product;
    List<ShopHasProductAggregated> shopHasProductAggregatedList;

    public ProductShopsBundle(Product product, List<ShopHasProductAggregated> shopHasProductAggregatedList) {
        this.product = product;
        this.shopHasProductAggregatedList = shopHasProductAggregatedList;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public List<ShopHasProductAggregated> getShopHasProductAggregatedList() {
        return shopHasProductAggregatedList;
    }

    public void setShopHasProductAggregatedList(List<ShopHasProductAggregated> shopHasProductAggregatedList) {
        this.shopHasProductAggregatedList = shopHasProductAggregatedList;
    }
}
