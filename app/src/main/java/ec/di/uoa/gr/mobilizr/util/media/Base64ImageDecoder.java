package ec.di.uoa.gr.mobilizr.util.media;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;

public class Base64ImageDecoder {
    public static SerialBitmap decodeBase64ImageToBitmap(String base64EncodedImageStr) {
        byte[] decodedImageBytes = Base64.decode(base64EncodedImageStr, Base64.DEFAULT);
        return new SerialBitmap(Bitmap.createBitmap(
                BitmapFactory.decodeByteArray(decodedImageBytes, 0, decodedImageBytes.length)));
    }
}
