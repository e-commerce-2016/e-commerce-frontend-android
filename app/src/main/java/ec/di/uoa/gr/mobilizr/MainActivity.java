package ec.di.uoa.gr.mobilizr;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import ec.di.uoa.gr.mobilizr.products.ProductsView;
import ec.di.uoa.gr.mobilizr.products.product.ProductView;
import ec.di.uoa.gr.mobilizr.products.product.entities.Product;
import ec.di.uoa.gr.mobilizr.products.search.SearchProductsView;
import ec.di.uoa.gr.mobilizr.shops.ShopView;
import ec.di.uoa.gr.mobilizr.shops.entities.Comment;
import ec.di.uoa.gr.mobilizr.shops.entities.ProductShopsBundle;
import ec.di.uoa.gr.mobilizr.shops.entities.Shop;
import ec.di.uoa.gr.mobilizr.util.media.Base64ImageDecoder;
import ec.di.uoa.gr.mobilizr.wishlist.WishlistActivity;
import timber.log.Timber;

public class MainActivity
        extends AppCompatActivity
        implements SearchProductsView.OnSearchResultsReceivedListener,
        ProductsView.OnProductShopsBundleReadyListener,
        ProductView.OnShopCommentsReadyListener {
    private View mRootView;
    private LatLng mLatLng;
//    ResultsCache resultsCache;
    private LocationRequest mLocationRequest;
    private LocationManager mLocationManager;
    private LocationListener mLocationListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
            Timber.tag("MobilizrApplication");
        }

//        resultsCache = ResultsCache.getInstance(getApplicationContext());
//        resultsCache.deSerializeData();

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        assert getSupportActionBar() != null;
        MobilizrApplication.setsActionBar(getSupportActionBar());

        mRootView = findViewById(R.id.main_activity_layout);

        // Acquire a reference to the system Location Manager
        mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        // Define a listener that responds to location updates
        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Timber.v("onLocationChanged: %s", location.toString());
                mLatLng = new LatLng(location.getLatitude(), location.getLongitude());
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {
            }
        };

        getPermissionToAccessLocation();

        // Ensure activity is using the fragment_main_container layout
        if (findViewById(R.id.fragment_main_container) != null) {

            // If we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

//            boolean shouldContinueExecution = cachingWrapper();
//            if (!shouldContinueExecution)
//                return;

            // Inflate the fragment main container layout with the SearchProductsView fragment.
            loadSearchProductView(false);
            return;
        }

//        cachingWrapper();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_wishlist) {
            Intent intent = new Intent(this, WishlistActivity.class);
            startActivity(intent);
            return true;
//        } else if (id == R.id.action_clear_cache) {
//            if (findViewById(R.id.fragment_search_layout) == null) {
//                resultsCache.clearCache();
//                Toast.makeText(getApplicationContext(),
//                        R.string.cache_cleared_message, Toast.LENGTH_LONG).show();
//                loadSearchProductView(true);
//            }
//            return true;
//        } else if (id == R.id.action_data_manager) {
//            Intent dbmanager = new Intent(this, ec.di.uoa.gr.mobilizr.data.AndroidDatabaseManager.class);
//            startActivity(dbmanager);
//        } else if (id == R.id.action_map) {
//            Intent intent = new Intent(this, MapActivity.class);
//            startActivity(intent);
        } else if (id == android.R.id.home) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStack();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void getPermissionToAccessLocation() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION)) {

                Snackbar.make(mRootView, getString(R.string.permission_location_rationale),
                        Snackbar.LENGTH_INDEFINITE)
                        .setAction(R.string.ok, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ActivityCompat.requestPermissions(MainActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MapActivity.MY_PERMISSIONS_REQUEST_LOCATION_INFO);
                            }
                        })
                        .show();
            } else {
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                        MapActivity.MY_PERMISSIONS_REQUEST_LOCATION_INFO);
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
//        if (resultsCache != null) {
//            resultsCache.serializeData();
//            Timber.v("Serialized available data.");
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadSearchProductView(true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MapActivity.MY_PERMISSIONS_REQUEST_LOCATION_INFO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    try {
                        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10 * 1000, 20, mLocationListener);
                    } catch (SecurityException e) {
                        Timber.e(e, "Mobilizr doesn't have the necessary permissions for accessing " +
                                "user's location although user accepted permission.");
                    }
                } else {
                    mLatLng = null;
                }
                break;
            }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public LatLng getLatLng() {
        if (mLatLng != null)
            return mLatLng;
        else {
            try {
                Location location = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                mLatLng = new LatLng(location.getLatitude(), location.getLongitude());
            } catch (SecurityException e) {
                Timber.e(e, "Mobilizr doesn't have the necessary permissions for accessing " +
                        "user's location although user accepted permission.");
            }
            return mLatLng;
        }
    }

    /**
     * Loads SearchProductView (aka main fragment).
     *
     * @param shouldClearBackStack If true, fragments' back stack is popped, otherwise,
     *                             SearchProductView fragment is simply loaded.
     */
    public void loadSearchProductView(boolean shouldClearBackStack) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if (shouldClearBackStack) {
            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            transaction.setCustomAnimations(R.anim.pop_enter, R.anim.pop_exit)
                    .add(R.id.fragment_main_container, new SearchProductsView()).commit();
        } else {
            transaction.add(R.id.fragment_main_container, new SearchProductsView())
                    .commit();
        }
    }

    // Event callbacks for handling communication between fragments --------------------------------

    /**
     * Event callback for handling the communication between SearchProductsView and ProductsView
     * fragments.
     *
     * @param productsList The ArrayList containing the products that the search returned.
     */
    @Override
    public void onSearchResultsReceived(ArrayList<Product> productsList) {
        Timber.v("Got reply from server: in total there are %d products",
                productsList.size());

//        if (!resultsCache.isProductsListSet())
//            resultsCache.setProducts(productsList);

        decodeBase64ImagesToBitmaps(productsList);

        // Create ProductsView fragment and set up its arguments.
        ProductsView productsView = ProductsView.newInstance(
                getString(R.string.products_search_results_list_key), productsList
        );
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter, R.anim.exit)
                .replace(R.id.fragment_main_container, productsView)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onProductShopsBundleReady(ProductShopsBundle productShopsBundle) {
        Timber.v("Got reply from server: in total there are %d shops offering %s",
                productShopsBundle.getShopHasProductAggregatedList().size(),
                productShopsBundle.getProduct().getName());

//        if (!resultsCache.isProductShopsBundleSet())
//            resultsCache.setProductShopsBundle(productShopsBundle);

        ProductView productView = ProductView.newInstance(
                getString(R.string.product_shops_bundle_key),
                productShopsBundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter, R.anim.exit)
                .replace(R.id.fragment_main_container, productView)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onShopCommentsReady(Shop shop, ArrayList<Comment> comments) {
        Timber.v("Got reply from server: in total there are %d comments for Shop: %s",
                comments.size(), shop.getBrand());

        ShopView shopView = ShopView.newInstance(
                getString(R.string.shop_view_shop_argument_key), shop,
                getString(R.string.shop_view_shop_comments_argument_key), comments);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter, R.anim.exit)
                .replace(R.id.fragment_main_container, shopView)
                .addToBackStack(null)
                .commit();
    }

    // Helpers -------------------------------------------------------------------------------------
    public static void decodeBase64ImagesToBitmaps(ArrayList<Product> productList) {
        // Decode base64EncodedImage strings to Bitmaps
        for (Product product : productList) {
            if (product.getBase64EncodedImageStr() != null &&
                    !product.getBase64EncodedImageStr().isEmpty())
                product.setImageBitmap(
                        Base64ImageDecoder.decodeBase64ImageToBitmap(
                                product.getBase64EncodedImageStr()
                        )
                );
            else
                // Set ImageStr to null so that ProductsView knows that product image
                // wasn't available from the backend endpoint.
                product.setBase64EncodedImageStr(null);
        }
    }

    /**
     * Caching can be disabled (from overwriting a fragment) by commenting-out the usage of this
     * method.
     * The default caching-load policy, as show below, attempts to load the latest fragment, and
     * if no data for it can be found, then the previous one, and so on and so forth.
     *
     * @return Returns true if callee must continue, false otherwise.
     * I.e. true, means that some fragment was already loaded, so OnCreate must return
     * to avoid overlapping fragments.
     */
//    private boolean cachingWrapper() {
//        // CachingResults can be disabled from overwriting a fragment by simply commenting out the
//        // two code-blocks below.
//
//        // Loads ProductView Fragment with Nexus 5X's data.
//        if (resultsCache.isProductShopsBundleSet()) {
//            onProductShopsBundleReady(resultsCache.getProductShopsBundle());
//            return false;
//        } else {
//            Timber.v("ShopBundle hasn't been initialized yet. Use the app until the ProductView " +
//                    "fragment.");
//        }
//
//        // Loads ProductView Fragment with Nexus 5X's data.
//        if (resultsCache.isProductsListSet()) {
//            onSearchResultsReceived(resultsCache.getProducts());
//            return false;
//        } else {
//            Timber.v("ProductsList hasn't been initialized yet. Use the app until the ProductView " +
//                    "fragment.");
//        }
//        return true;
//    }
}
