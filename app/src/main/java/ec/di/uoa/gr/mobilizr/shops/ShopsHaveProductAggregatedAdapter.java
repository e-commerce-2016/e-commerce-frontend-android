package ec.di.uoa.gr.mobilizr.shops;

import android.animation.ValueAnimator;
import android.graphics.drawable.TransitionDrawable;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import ec.di.uoa.gr.mobilizr.MainActivity;
import ec.di.uoa.gr.mobilizr.R;
import ec.di.uoa.gr.mobilizr.products.product.SelectProductAsyncTask;
import ec.di.uoa.gr.mobilizr.products.product.entities.ProductViewAsyncTaskBundle;
import ec.di.uoa.gr.mobilizr.shops.entities.Shop;
import ec.di.uoa.gr.mobilizr.shops.entities.ShopHasProduct;
import ec.di.uoa.gr.mobilizr.shops.entities.ShopHasProductAggregated;

public class ShopsHaveProductAggregatedAdapter extends ArrayAdapter<ShopHasProductAggregated>
    implements SelectProductAsyncTask.OnOutcomeReadyListener {
    private OnShopHasProductSelectedListener mOnShopHasProductSelectedListener;
    private OnShopSelectedListener mOnShopSelectedListener;
    private double toastHeight;

    public ShopsHaveProductAggregatedAdapter(Fragment clientFragment,
                                             int resource,
                                             List<ShopHasProductAggregated> objects) {
        super(clientFragment.getContext(), resource, objects);
        try {
            mOnShopHasProductSelectedListener = (OnShopHasProductSelectedListener) clientFragment;
        } catch (ClassCastException e) {
            throw new RuntimeException(clientFragment.getClass().toString() + " must implement " +
                    OnShopHasProductSelectedListener.class.toString() + " interface");
        }
        try {
            mOnShopSelectedListener = (OnShopSelectedListener) clientFragment;
        } catch (ClassCastException e) {
            throw new RuntimeException(clientFragment.getClass().toString() + " must implement " +
                    OnShopSelectedListener.class.toString() + " interface");
        }

        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        toastHeight = displayMetrics.heightPixels * 0.05;
    }

    private static class ViewHolder {
        View rootView;
        // Title/Layout
        RelativeLayout shopNameRatingLayout;
        TextView shopBrandTextView;
        RatingBar shopRatingRatingBar;
        TextView shopRatingTextView;
        // Shop and ShopHasProduct details layout
        //      Shop information
        LinearLayout productViewDetailsLayout;
        LinearLayout shopDetailsLayout;
        TextView shopLocationTextView;
        TextView shopPhonesTextView;
        //      ShopHasProduct information
        LinearLayout shopHasProductDetailsLayout;
        TextView shopHasProductPriceTextView;
        TextView shopHasProductQuantityTextView;
        //      Buttons
        RelativeLayout buttonsLayout;
        Button addToWishlistBtn;
        Button openShopViewButton;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ShopHasProductAggregated shopHasProductAggregated = getItem(position);

        final ViewHolder viewHolder;  // View lookup cache is stored in View.Tag.

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext())
                    .inflate(R.layout.product_view_shops_list_item, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.rootView = convertView;
            // Shop and ShopHasProduct Label/Title layout ------------------------------------------
            // Shop and Rating information ---------------------------------------------------------
            viewHolder.shopNameRatingLayout = (RelativeLayout)
                    convertView.findViewById(R.id.shop_name_rating_layout);
            viewHolder.shopBrandTextView = (TextView)
                    viewHolder.shopNameRatingLayout.findViewById(R.id.shop_brand_textview);

            // Rating handling ---------------------------------------------------------------------
            viewHolder.shopRatingRatingBar = (RatingBar)
                    viewHolder.shopNameRatingLayout.findViewById(R.id.shop_rating_ratingbar);
            viewHolder.shopRatingTextView = (TextView)
                    viewHolder.shopNameRatingLayout.findViewById(R.id.shop_rating_textview);

            // Shop and ShopHasProduct details layout ----------------------------------------------
            viewHolder.productViewDetailsLayout = (LinearLayout)
                    convertView.findViewById(R.id.product_view_details_layout);

            viewHolder.shopDetailsLayout = (LinearLayout)
                    viewHolder.productViewDetailsLayout.findViewById(R.id.shop_details_layout);

            viewHolder.shopLocationTextView = (TextView)
                    viewHolder.shopDetailsLayout.findViewById(R.id.shop_location);

            viewHolder.shopPhonesTextView = (TextView)
                    viewHolder.shopDetailsLayout.findViewById(R.id.shop_phones);


            // ShopHasProduct information ----------------------------------------------------------
            viewHolder.shopHasProductDetailsLayout = (LinearLayout)
                    convertView.findViewById(R.id.shop_has_product_details_layout);
            viewHolder.shopHasProductPriceTextView = (TextView)
                    viewHolder.shopHasProductDetailsLayout.findViewById(R.id.shop_has_product_price);
            viewHolder.shopHasProductQuantityTextView = (TextView)
                    viewHolder.shopHasProductDetailsLayout.findViewById(R.id.shop_has_product_quantity);

            // ProductView ListView Item Buttons
            viewHolder.buttonsLayout = (RelativeLayout)
                    viewHolder.rootView.findViewById(R.id.product_view_list_buttons);

            viewHolder.addToWishlistBtn = (Button)
                    viewHolder.buttonsLayout.findViewById(R.id.add_to_wishlist_button);

            viewHolder.openShopViewButton = (Button)
                    viewHolder.buttonsLayout.findViewById(R.id.open_shop_view_button);

            // Cache the view holder inside the fresh view
            convertView.setTag(viewHolder);
        }
        else
            // View is being recycled, retrieve the viewHolder object from tag
            viewHolder = (ViewHolder) convertView.getTag();

        // Setting view data
        viewHolder.shopBrandTextView.setText(shopHasProductAggregated.getShop().getBrand());

        if (shopHasProductAggregated.getShop().getRatingCount() != 0) {
            long shopRating = shopHasProductAggregated.getShop().getRatingSum() /
                    shopHasProductAggregated.getShop().getRatingCount();
            viewHolder.shopRatingRatingBar.setRating(shopRating);
        } else {
            viewHolder.shopRatingRatingBar.setVisibility(View.GONE);
            viewHolder.shopRatingTextView.setVisibility(View.VISIBLE);
        }

        String shopLocation = shopHasProductAggregated.getShop().getAddress() + ", " +
                shopHasProductAggregated.getShop().getCity();
        viewHolder.shopLocationTextView.setText(shopLocation);

        String shopPhones = shopHasProductAggregated.getShop().getPhone();
        if (shopHasProductAggregated.getShop().getMobilePhone() != null)
            shopPhones += ", " + shopHasProductAggregated.getShop().getMobilePhone();
        viewHolder.shopPhonesTextView.setText(shopPhones);

        String priceStr = Double.toString(shopHasProductAggregated.getShopHasProduct().getPrice())
                + " " + getContext().getString(R.string.euro_sign);
        viewHolder.shopHasProductPriceTextView.setText(priceStr);

        String quantity = shopHasProductAggregated.getShopHasProduct().getQuantity().toString();
        viewHolder.shopHasProductQuantityTextView.setText(quantity);

        // Event Listeners -------------------------------------------------------------------------
        viewHolder.addToWishlistBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(getContext(),
                        getContext().getString(R.string.add_to_wishlist_button_explanation),
                        Toast.LENGTH_LONG).show();
                return true;
            }
        });

        viewHolder.addToWishlistBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnShopHasProductSelectedListener
                        .onShopHasProductSelected(
                                new ProductViewAsyncTaskBundle(
                                        shopHasProductAggregated, null,
                                        ((View)v.getParent().getParent())));
            }
        });

        viewHolder.openShopViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnShopSelectedListener.OnShopSelected(getItem(position).getShop());
            }
        });

        return convertView;
    }

    // UI Helpers ----------------------------------------------------------------------------------
    /**
     * Ugly code making beautiful animation for user confirmation of adding a product to the
     * WishList.
     */
    private void handleSuccessConfirmation(Button button) {
        final Button btn = button;
        // Fast reaction ---------------------------------------------------------------------------
        final int fastReactionDuration = 350;
        // Background transition
        final TransitionDrawable transition = (TransitionDrawable) btn.getBackground();
        transition.startTransition(fastReactionDuration);
        // Update text
        btn.setText(getContext().getString(R.string.added_to_wishlist_btn_text));
        // Make button bigger
        final int paddingTop = btn.getPaddingTop();
        final int paddingBottom = btn.getPaddingBottom();
        final int paddingLeft = btn.getPaddingLeft();
        final int paddingRight = btn.getPaddingRight();
        final ValueAnimator enlargeButtonAnimator = ValueAnimator.ofInt(paddingRight, 18);
        enlargeButtonAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                btn.setPadding(paddingLeft, paddingTop,
                        (Integer) enlargeButtonAnimator.getAnimatedValue(), paddingBottom);
            }
        });
        enlargeButtonAnimator.setDuration(fastReactionDuration).start();

        // Slow restoration ------------------------------------------------------------------------
        final int restorationStartDelay = 3000;
        final int restorationDuration = 4000;
        Timer buttonRestorationTimer = new Timer();

        final ValueAnimator reduceButtonAnimator = ValueAnimator.ofInt(18, paddingRight);
        reduceButtonAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                btn.setPadding(paddingLeft, paddingTop,
                        (Integer) reduceButtonAnimator.getAnimatedValue(), paddingBottom);
            }
        });
        reduceButtonAnimator.setDuration(restorationDuration).setStartDelay(restorationStartDelay);

        buttonRestorationTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                final Animation shake = AnimationUtils.loadAnimation(getContext(), R.anim.shake_button);

                MainActivity mainActivity = (MainActivity) getContext();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btn.startAnimation(shake);
                        transition.reverseTransition(restorationDuration + 1000);
                        btn.setText(getContext().getString(R.string.add_to_wishlist_btn_label));
                    }
                });
            }
        }, restorationStartDelay - 1000);

        showToast(Toast.LENGTH_LONG, R.layout.success_toast, R.string.all_is_fine_all_is_well);
    }

    /**
     * Displays a toast message defined in the custom toastLayoutId layout.
     * @param toastDuration Obtained from Toast.LENGH_{SHORT, LONG}
     * @param toastLayoutId The id of the custom toast layout (R.layout.X)
     */
    private void showToast(int toastDuration, int toastLayoutId, int strMessageID) {
        Toast t = Toast.makeText(getContext(), "", toastDuration);
        // Load custom toast view
        View view = LayoutInflater.from(getContext()).inflate(
                getContext().getResources().getLayout(toastLayoutId), null);
        ((TextView)view).setText(strMessageID);
        t.setView(view);
        t.setGravity(Gravity.BOTTOM | Gravity.CENTER_VERTICAL, 0, (int) toastHeight);
        t.show();
    }

    @Override
    public void onOutcomeReady(ProductViewAsyncTaskBundle bundle) {
        StatusCode statusCode = bundle.getStatusCode();
        switch (statusCode) {
            case NO_AVAILABILITY:
                showToast(Toast.LENGTH_LONG, R.layout.warning_toast, R.string.no_stock_currently_available);
                remove(bundle.getShopHasProductAggregated());
                break;
            case FAILURE:
                showToast(Toast.LENGTH_LONG, R.layout.failure_toast, R.string.something_went_wrong_message);
                break;
            case SUCCESS:
                ShopHasProduct sHP = bundle.getShopHasProductAggregated().getShopHasProduct();
                sHP.setQuantity(sHP.getQuantity() - 1);
                notifyDataSetChanged();
                Button addToWishlistBtn = ((ViewHolder) bundle.getAdapterViewItem().getTag()).addToWishlistBtn;
                handleSuccessConfirmation(addToWishlistBtn);
                break;
        }
    }

    public interface OnShopHasProductSelectedListener {
        /**
         * Callback method for supplying selected data to the interface implementation class
         */
        void onShopHasProductSelected(ProductViewAsyncTaskBundle bundle);
    }

    public interface OnShopSelectedListener {
        /**
         * Event callback for notifying fragment to fetch shop related data and open ShopView.
         * @param shop  The shop whose info are to be displayed.
         */
        void OnShopSelected(Shop shop);
    }

}
