package ec.di.uoa.gr.mobilizr.shops;

import android.os.AsyncTask;

import ec.di.uoa.gr.mobilizr.MobilizrApplication;
import ec.di.uoa.gr.mobilizr.data.MobilizrDbHelper;
import ec.di.uoa.gr.mobilizr.data.models.WishlistBundle;
import ec.di.uoa.gr.mobilizr.evaluation.ShopEvaluationView;
import ec.di.uoa.gr.mobilizr.shops.entities.ShopHasProductAggregated;
import ec.di.uoa.gr.mobilizr.shops.soap.FJDFeedbackHandlerImplementationPortBinding;
import ec.di.uoa.gr.mobilizr.util.BackendUtils;
import timber.log.Timber;

public class FeedbackAsyncTask extends AsyncTask<Void, Void, Boolean>{

    private static final String sNAMESPACE = "http://soap.webservices.feedback.mobilizr.com/";
    private static final String sURL = "http://" +
            BackendUtils.getBackendIP(MobilizrApplication.getContext()) +
            ":8080/mobilizr/ws/FeedbackHandlerImplementation?wsdl";
    private static final String sMETHOD_NAME = "insertFeedback";
    private static final String sSOAP_ACTION = "http://soap.webservices.feedback.mobilizr.com/FeedbackHandler/postFeedback";

    private final long productId;
    private final long shopId;
    private final String comment;
    private final long rating;
    private final double realPrice;
    private WishlistBundle mWishlistBundle;
    private OnFeedbackOutcomeReadyListener mOnFeedbackOutcomeReadyListener;

    public FeedbackAsyncTask(
            ShopEvaluationView clientFragment,
            WishlistBundle wishlistBundle,
            String comment,
            long rating, double realPrice) {

        try {
            mOnFeedbackOutcomeReadyListener = (OnFeedbackOutcomeReadyListener) clientFragment;
        }
        catch (ClassCastException e) {
            throw new ClassCastException(clientFragment.getClass().getCanonicalName()
                    + " must implement " + OnFeedbackOutcomeReadyListener.class.getCanonicalName()
                    + " interface: " + e.getMessage());
        }

        this.shopId = wishlistBundle.getShop().getId();
        this.productId = wishlistBundle.getProduct().getId();
        this.comment = comment;
        this.rating = rating == 0 ? 1 : rating;
        this.realPrice = realPrice;
        this.mWishlistBundle = wishlistBundle;
    }

    @Override
    protected Boolean doInBackground(Void... params) {

        FJDFeedbackHandlerImplementationPortBinding service =
                new FJDFeedbackHandlerImplementationPortBinding(sURL);

        try {
            // Try to post feedback
            Boolean postedFeedback;
            if (realPrice != -1)
                postedFeedback = service.postFeedback(shopId, productId, comment, rating, realPrice);
            else
                postedFeedback = service.postFeedback(shopId, productId, comment, rating, mWishlistBundle.getWishlistModel().getPrice());

            // Persist wishlist item with SET_BOUGHT
            if (postedFeedback) {
                // Prepare wishlist model
                mWishlistBundle.getWishlistModel().setWasBought(true);
                if (realPrice != -1)
                    mWishlistBundle.getWishlistModel().setPrice(realPrice);

                return MobilizrApplication.getDbHelper()
                        .addOrUpdateWishlist(mWishlistBundle.getWishlistModel(),
                                null, new ShopHasProductAggregated(), MobilizrDbHelper.QUANTITY_HANDLING_MODE.SET_BOUGHT);
            }
            else {
                Timber.e("Sth went wrong with the backend.");
                return false;
            }

        } catch (Exception e) {
            Timber.e(e, "PostFeedback failed.");
            return null;
        }
    }


    @Override
    protected void onPostExecute(Boolean result) {
        mOnFeedbackOutcomeReadyListener.onOutcomeReady(result);
    }

    public interface OnFeedbackOutcomeReadyListener {
        void onOutcomeReady(Boolean wasSuccessful);
    }

}
