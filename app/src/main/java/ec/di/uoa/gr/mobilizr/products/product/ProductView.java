package ec.di.uoa.gr.mobilizr.products.product;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SlidingPaneLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ec.di.uoa.gr.mobilizr.MainActivity;
import ec.di.uoa.gr.mobilizr.MobilizrApplication;
import ec.di.uoa.gr.mobilizr.R;
import ec.di.uoa.gr.mobilizr.products.product.entities.ProductViewAsyncTaskBundle;
import ec.di.uoa.gr.mobilizr.shops.FetchShopCommentsTask;
import ec.di.uoa.gr.mobilizr.shops.ShopsHaveProductAggregatedAdapter;
import ec.di.uoa.gr.mobilizr.shops.entities.Comment;
import ec.di.uoa.gr.mobilizr.shops.entities.ProductShopsBundle;
import ec.di.uoa.gr.mobilizr.shops.entities.Shop;
import ec.di.uoa.gr.mobilizr.shops.entities.ShopHasProductAggregated;
import ec.di.uoa.gr.mobilizr.util.BackendUtils;
import ec.di.uoa.gr.mobilizr.util.OnArrayListAsyncTaskCompletedListener;
import ec.di.uoa.gr.mobilizr.util.UiUtils;
import timber.log.Timber;

public class ProductView extends Fragment
        implements ShopsHaveProductAggregatedAdapter.OnShopHasProductSelectedListener,
        ShopsHaveProductAggregatedAdapter.OnShopSelectedListener {
    @BindView(R.id.radioFilterbyDistance)
    RadioButton radioFilterbyDistance;
    @BindView(R.id.spinnerFilterbyDistance)
    Spinner spinnerFilterbyDistance;
    @BindView(R.id.radioFilterbyTime)
    RadioButton radioFilterbyTime;
    @BindView(R.id.spinnerFilterbyTime)
    Spinner spinnerFilterbyTime;
    @BindView(R.id.radioFilterbyRating)
    RadioButton radioFilterbyRating;
    @BindView(R.id.radioFilter)
    RadioGroup radioFilter;
    private ProductShopsBundle mProductShopsBundle;
    private List<ShopHasProductAggregated> mShopHasProductAggregatedBackupList;
    private ShopsHaveProductAggregatedAdapter mShopsHaveProductAggregatedAdapter = null;
    private ArrayAdapter<String> mArrayAdapter;
    private OnShopCommentsReadyListener mOnShopCommentsReadyListener;
    private MainActivity mMainActivity;
    private SlidingPaneLayout mSlidingPaneRootView;


    public ProductView() {
        // Required empty public constructor
    }

    public static ProductView newInstance(
            String productShopsBundleKey,
            ProductShopsBundle productShopsBundle) {
        ProductView fragment = new ProductView();
        Bundle args = new Bundle();
        args.putSerializable(productShopsBundleKey, productShopsBundle);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mProductShopsBundle = (ProductShopsBundle) getArguments()
                    .get(getString(R.string.product_shops_bundle_key));
            mShopHasProductAggregatedBackupList = new ArrayList<>();
            mShopHasProductAggregatedBackupList.addAll(mProductShopsBundle.getShopHasProductAggregatedList());
        }
        if (mProductShopsBundle != null)
            mShopsHaveProductAggregatedAdapter = new ShopsHaveProductAggregatedAdapter(
                    this,
                    R.layout.product_view_shops_list_item,
                    mProductShopsBundle.getShopHasProductAggregatedList());

        mMainActivity = (MainActivity) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mSlidingPaneRootView = (SlidingPaneLayout)
                inflater.inflate(R.layout.product_view_container_layout, container, false);
        ButterKnife.bind(this, mSlidingPaneRootView);

        MobilizrApplication.getsActionBar().setTitle(R.string.toolbar_title_product);
        MobilizrApplication.getsActionBar().setDisplayHomeAsUpEnabled(true);

        // SlidingPaneLayout related ---------------------------------------------------------------
        mSlidingPaneRootView.setParallaxDistance((int) getResources().getDimension(R.dimen.filters_sliding_drawer_width));
        spinnerFilterbyTime.setEnabled(false);

        // Product related -------------------------------------------------------------------------
        RelativeLayout productContainerView = (RelativeLayout)
                mSlidingPaneRootView.findViewById(R.id.product_view_product_container);

        ImageView productImageView = (ImageView)
                productContainerView.findViewById(R.id.product_view_product_image);

        if (mProductShopsBundle.getProduct().getImageBitmap() != null)
            productImageView.setImageBitmap(mProductShopsBundle.getProduct().getImageBitmap().getBitmap());
        else
            // Couldn't retrieve image bitmap, display the ic_error_black icon.
            productImageView.setImageResource(R.drawable.ic_error_black_24dp);

        TextView productNameTextView = (TextView)
                productContainerView.findViewById(R.id.product_view_product_name);
        productNameTextView.setText(mProductShopsBundle.getProduct().getName());

        TextView productDescriptionTextView = (TextView)
                productContainerView.findViewById(R.id.product_view_product_description);
        productDescriptionTextView.setText(mProductShopsBundle.getProduct().getDescription());

        // Shops related ---------------------------------------------------------------------------
        ListView shopsListView = (ListView) mSlidingPaneRootView.findViewById(R.id.product_view_shops_listview);
        shopsListView.setAdapter(mShopsHaveProductAggregatedAdapter);

        // Show filters drawer button
        FloatingActionButton showFiltersButton = (FloatingActionButton)
                mSlidingPaneRootView.findViewById(R.id.show_filters_button);
        showFiltersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mSlidingPaneRootView.isOpen())
                    mSlidingPaneRootView.closePane();
                else
                    mSlidingPaneRootView.openPane();
            }
        });

        return mSlidingPaneRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity currentActivity = getActivity();
        try {
            mOnShopCommentsReadyListener = (OnShopCommentsReadyListener) currentActivity;
        } catch (ClassCastException e) {
            throw new ClassCastException(currentActivity.toString() + " must implement " +
                    OnShopCommentsReadyListener.class);
        }
    }

    // Event listeners -----------------------------------------------------------------------------
    @Override
    public void onShopHasProductSelected(ProductViewAsyncTaskBundle bundle) {
        String selection = mProductShopsBundle.getProduct().getName() + " -> " +
                bundle.getShopHasProductAggregated().getShop().getBrand() + " @ " +
                bundle.getShopHasProductAggregated().getShopHasProduct().getPrice();
        Timber.v("User selected: %s", selection);

        // Attempt to update product quantity at given shop and persist to storage.
        new SelectProductAsyncTask(this,
                bundle,
                mProductShopsBundle.getProduct(),
                MobilizrApplication.getDbHelper())
                .execute();
    }

    @Override
    public void OnShopSelected(final Shop shop) {
        Integer commentsGetEndpointIdx = getResources().getInteger(R.integer.commentsRS);
        String endpointUrl =
                getResources().getStringArray(R.array.webServicesEntrypoints)[commentsGetEndpointIdx];
        String endpoint = BackendUtils.prepareEndpointURL(getContext(), endpointUrl);
        String shopId = shop.getId().toString();
        Uri builtUri = Uri.parse(endpoint).buildUpon()
                .appendPath(shopId).build();

        new FetchShopCommentsTask(getContext(), new OnArrayListAsyncTaskCompletedListener<Comment>() {
            @Override
            public void onAsyncTaskCompleted(ArrayList<Comment> resultsArrayList) {
                if (resultsArrayList == null) {
                    final Snackbar snackbar =
                            Snackbar.make(mSlidingPaneRootView, R.string.connection_error_message,
                                    Snackbar.LENGTH_INDEFINITE);

                    snackbar.setAction(R.string.dismiss_action,
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    snackbar.dismiss();
                                }
                            });
                    snackbar.show();
                    return;
                }
                Timber.v("Fetched %d comments.", resultsArrayList.size());
                mOnShopCommentsReadyListener.onShopCommentsReady(shop, resultsArrayList);
            }
        }).execute(builtUri);
    }

    @OnClick({R.id.radioFilterbyDistance, R.id.radioFilterbyTime, R.id.radioFilterbyRating, R.id.filters_apply_button})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.radioFilterbyDistance:
                spinnerFilterbyTime.setEnabled(false);
                spinnerFilterbyDistance.setEnabled(true);
                break;
            case R.id.radioFilterbyTime:
                spinnerFilterbyDistance.setEnabled(false);
                spinnerFilterbyTime.setEnabled(true);
                break;
            case R.id.radioFilterbyRating:
                spinnerFilterbyDistance.setEnabled(false);
                spinnerFilterbyTime.setEnabled(false);
                break;
            case R.id.filters_apply_button:
                int checkedRadioButtonId = radioFilter.getCheckedRadioButtonId();
                switch (checkedRadioButtonId) {
                    case R.id.radioFilterbyDistance:
                        if (mMainActivity.getLatLng() == null) {
                            UiUtils.snackBarNotify(mSlidingPaneRootView, getString(R.string.location_not_available_yet));
                        }
                        else
                            filterByDistance(
                                    Double.parseDouble(spinnerFilterbyDistance.getSelectedItem().toString()),
                                    mMainActivity.getLatLng().latitude,
                                    mMainActivity.getLatLng().longitude);
                        break;
                    case R.id.radioFilterbyTime:
                        if (mMainActivity.getLatLng() == null) {
                            UiUtils.snackBarNotify(mSlidingPaneRootView, getString(R.string.location_not_available_yet));
                        }
                        else
                            filterByArrivalTime(
                                Integer.parseInt(spinnerFilterbyTime.getSelectedItem().toString()),
                                mMainActivity.getLatLng().latitude,
                                mMainActivity.getLatLng().longitude);
                        break;
                    case R.id.radioFilterbyRating:
                        filterByRating();
                        break;
                }
                mSlidingPaneRootView.closePane();
                break;
        }
    }

    // ---------------------------------------------------------------------------------------------
    public interface OnShopCommentsReadyListener {
        void onShopCommentsReady(Shop shop, ArrayList<Comment> comments);
    }
    // ---------------------------------------------------------------------------------------------

    /**
     * Filters shops that are located nearby within given radius
     *
     * @param radius    radius in km
     * @param latitude  current latitude
     * @param longitude current longitude
     */
    public void filterByDistance(
            double radius, double latitude, double longitude) {
        mProductShopsBundle.getShopHasProductAggregatedList().clear();

        for (ShopHasProductAggregated shopProduct : mShopHasProductAggregatedBackupList) {
            Shop shop = shopProduct.getShop();

            // Get shop coordinates
            double shopLatitude = shop.getLat();
            double shopLongitude = shop.getLon();

            // Compute shop distance in km
            double distance = distance(shopLatitude, shopLongitude, latitude, longitude);

            // Add item if within radius
            if (distance <= radius)
                mProductShopsBundle.getShopHasProductAggregatedList().add(shopProduct);
        }
        // Ensuring that filters won't exclude all shops
        if (mProductShopsBundle.getShopHasProductAggregatedList().size() == 0) {
            UiUtils.snackBarNotify(mSlidingPaneRootView, "Your filters returned no results. Won't apply filtering.");
            mProductShopsBundle.getShopHasProductAggregatedList().clear();
            mProductShopsBundle.getShopHasProductAggregatedList().addAll(mShopHasProductAggregatedBackupList);
        }
        else
            getmShopsHaveProductAggregatedAdapter().notifyDataSetChanged();
    }

    /**
     * Filters shops that are located nearby
     *
     * @param eta       estimated time of arrival
     * @param latitude  current latitude
     * @param longitude current longitude
     */
    public void filterByArrivalTime(
            double eta, double latitude, double longitude) {
        mProductShopsBundle.getShopHasProductAggregatedList().clear();

        for (ShopHasProductAggregated shopProduct : mShopHasProductAggregatedBackupList) {
            Shop shop = shopProduct.getShop();

            // Get shop coordinates
            double shopLatitude = shop.getLat();
            double shopLongitude = shop.getLon();

            // Compute shop distance in km
            double distance = distance(shopLatitude, shopLongitude, latitude, longitude);
            double defaultSpeed = 10.0; // km/hour
            double time = 60 * (distance / defaultSpeed);

            // Add item if within estimated time of arrival
            if (time <= eta)
                mProductShopsBundle.getShopHasProductAggregatedList().add(shopProduct);
        }

        // Ensuring that filters won't exclude all shops
        if (mProductShopsBundle.getShopHasProductAggregatedList().size() == 0) {
            UiUtils.snackBarNotify(mSlidingPaneRootView, "Your filters returned no results. Won't apply filtering.");
            mProductShopsBundle.getShopHasProductAggregatedList().clear();
            mProductShopsBundle.getShopHasProductAggregatedList().addAll(mShopHasProductAggregatedBackupList);
        }
        else
            getmShopsHaveProductAggregatedAdapter().notifyDataSetChanged();
    }

    /**
     * Filters shops that are located nearby.
     */
    public void filterByRating() {
        Collections.sort(mProductShopsBundle.getShopHasProductAggregatedList(), new Comparator<ShopHasProductAggregated>() {
            @Override
            public int compare(ShopHasProductAggregated lhs, ShopHasProductAggregated rhs) {
                double lhsRating = lhs.getShop().getRatingCount() != 0 ?
                        lhs.getShop().getRatingSum() / lhs.getShop().getRatingCount() : 0.0;
                double rhsRating = rhs.getShop().getRatingCount() != 0 ?
                        rhs.getShop().getRatingSum() / rhs.getShop().getRatingCount() : 0.0;
                // Purposely reversing values so as to get desc. order sorting.
                if (lhsRating < rhsRating) return 1;
                else if (lhsRating == rhsRating) return 0;
                else return -1;
            }
        });
        getmShopsHaveProductAggregatedAdapter().notifyDataSetChanged();
    }

    private static double distance(double lat1, double lon1, double lat2, double lon2) {
        final int R = 6371; // Radius of the earth

        double latDistance = Math.toRadians(lat2 - lat1);
        double lonDistance = Math.toRadians(lon2 - lon1);

        // See http://www.movable-type.co.uk/scripts/latlong.html for formula
        double lat1Radians = Math.toRadians(lat1);
        double lat2Radians = Math.toRadians(lat2);
        double x = Math.sin(latDistance / 2);
        double y = Math.sin(lonDistance / 2);

        double a = (x * x) + (Math.cos(lat1Radians) * Math.cos(lat2Radians) * y * y);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double distance = R * c;

        return distance;
    }

    public ShopsHaveProductAggregatedAdapter getmShopsHaveProductAggregatedAdapter() {
        return mShopsHaveProductAggregatedAdapter;
    }
}
